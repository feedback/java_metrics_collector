# Java Metrics Analyzer
A modular Java code smell finder

## How to use:
1. Run the command `gradle shadowjar` to produce a fatjar of the program
2. Execute the jar with `java -jar JavaMetrics-VERSION-all.jar <path/to/code> [path/to/settings.yaml]`
3. 4 CSV files (class, field, method, variable) are produced containing raw metrics
4. 1 JSON file (JavaCodeSmells) is produced, containing location of all code smells, insight on how/why to fix them,
and an overview containing the frequency of each found code smell

## How to edit smell settings
1. Navigate to src/main/resources
2. Open settings.yaml in a text editor or IDE, you will see a file that resembles the content below:
* `used`: If you want to turn a sniffer on or off, changed to true or false respectively
* `className`: Name of the Java class which looks for code smells; typically should not be changed
* `recommendation`: Information on how to fix the code smell
* `reasoning`: Information on why to fix the code smell
* `#`: A comment describing the calculation performed on code metrics to find the smell and whether a higher or lower value is considered more severe
* `minorThreshold`: The minimum value that must be passed by the calculation results for something to be a smell
* `moderateThreshold`: The value that must be passed by the calculation results for a smell to be considered moderate
* `majorThreshold`: The value that must be passed by the calculation results for a smell to be considered severe

###### Note: Each Sniffer's settings must start with a line containing "---" followed by "!!sniffer.SnifferSetting" on another line.

Example:
```yaml
---
!!sniffer.SnifferSetting
  used: True
  className: TooManyFields
  recommendation: This class and its fields should be broken up across multiple classes.
  reasoning: Too many instance variables leads to an object with too many states, which can result in many different methods with minor changes for each state (repeated code).
  # Number of fields (ascending)
  minorThreshold: 10
  moderateThreshold: 20
  majorThreshold: 30
---
!!sniffer.SnifferSetting
  used: true
  className: LongMethod
  recommendation: This method should be broken up using helper methods or other classes that perform some of the functionality. Maintain the single responsibility principle.
  reasoning: Too many lines of code in a single method reduce both modularity and readability of the method. The easiest to read and reuse methods are shorter.
  # Lines of code in method (ascending)
  minorThreshold: 20
  moderateThreshold: 30
  majorThreshold: 40
```

<br>
Author: Shawn Kaplan

Code Smell Source: https://sourcemaking.com/refactoring/smells
