# calculate metrics from a class set of submissions


# For each student

# make a metrics folder
# copy the desired files into sub folder
# java -jar ck-x.x.x-SNAPSHOT-jar-with-dependencies.jar <project dir> <use jars:true|false>
# rename the output files

# !/usr/bin/python
#
import argparse
import os
import shutil
import subprocess
from subprocess import Popen

from progress.bar import Bar

# DEF CONSTANTS
ASSIGNMENT_REPO = "a4"
STDNT_WORK_DIR = "subs1"
OUTPUT_DIR = "metrics"
# TODO I had to put the fully qualified pathname of the jar file here to make this work
JAR = "ck.jar"


def makeDir(dirname):
    if not os.path.isdir(dirname):
        subprocess.run(["mkdir", dirname])


def processOne(studentDir):
    cur = os.getcwd()
    # jar = "ck.jar"
    if (os.path.isdir(studentDir)):
        os.chdir(studentDir)
        # command = ["ls"]
        command = ["java", "-jar", JAR, "src"]
        p = Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)  # ,shell=True,stderror=subprocess.STDOUT,)
        out, err = p.communicate()
    # print(out)
    os.chdir(cur)


def walkRepos(target, repo):
    count = len(next(os.walk(target))[1])
    home = "/".join([os.getcwd(), target])
    os.chdir(target)  # target dir must exist
    bar = Bar('Processing', max=count)
    for student in os.listdir('.'):
        # print "working on " + student
        studentDir = "/".join([student, repo])
        processOne(studentDir)
        # print(studentDir)
        bar.next()
    bar.finish()


def moveFiles(source, target, student):
    targfiles = ['class.csv', 'field.csv', 'method.csv', 'variable.csv']
    cur = os.getcwd()
    if (os.path.isdir(source)):
        os.chdir(source)
        makeDir("/".join([target, student]))
        for filename in targfiles:
            if os.path.isfile(filename):
                newName = student + filename
                newPlace = "/".join([target, student, newName])
                shutil.move(filename, newPlace)
            # print(newPlace)
    os.chdir(cur)


def gatherData(source, target, repo):
    count = len(next(os.walk(source))[1])
    home = "/".join([os.getcwd(), source])
    os.chdir(source)  # target dir must exist
    bar = Bar('Processing', max=count)
    for student in os.listdir('.'):
        # print "working on " + student
        studentDir = "/".join([student, repo])
        moveFiles(studentDir, target, student)
        bar.next()
    bar.finish()


def main(args):
    # COMMAND LINE ARGS
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--assignmentRepo", type=str, help="Repo Name (just the directory)",
                        default=ASSIGNMENT_REPO)
    parser.add_argument("-d", "--studentWorkDirectory", type=str, help="Directory holding student repositories",
                        default=STDNT_WORK_DIR)
    args = parser.parse_args()

    # MAIN
    home = os.getcwd()
    outdir = "/".join([home, "metrics"])
    print("Calculating Metrics")
    walkRepos(args.studentWorkDirectory, args.assignmentRepo)
    os.chdir(home)
    print("GatheringResults")
    gatherData(args.studentWorkDirectory, outdir, args.assignmentRepo)

    return


if __name__ == '__main__':
    import sys

    sys.exit(main(sys.argv))
