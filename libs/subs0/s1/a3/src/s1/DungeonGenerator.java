package s1;
/**
 * This is the main class of the DND package. Based heavily on code used in the first assignment.
 * It simply signals other classes to run.
 * @author s1
 * @version 1.0
 * @since 2019-09-30
 */
final class DungeonGenerator {
    private DungeonGenerator() {
    }
    /**
     *  Runs the main program.
     *  @param args The arguments used with the main (unused in this program).
     */
    public static void main(String[] args) {
        int genType = 0;
        if (args.length >= 1) {
            try {
                genType = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.out.println("Enter a valid number as an argument.");
            }
        }
        Floor generateFloor = new Floor(genType);
        String printDescription = generateFloor.getDescription();
        System.out.println(printDescription);
    }
}
