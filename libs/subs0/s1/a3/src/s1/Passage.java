package s1;
import dnd.die.D20;
import dnd.models.Monster;

import java.util.ArrayList;
/**
 * This is the passage class of the ajetleb package. Utility is to build entire sections of passage.
 * @author s1
 * @version 1.2
 * @since 2019-10-04
 */
public class Passage extends Space {
    /**
     *  A variable that stores the collection of passagesections.
     */
    private ArrayList<PassageSection> thePassage;
    /**
     *  A variable that stores the collection of doors.
     */
    private ArrayList<Door> passageDoors;
    /**
     *  A variable containing the only door for the passage.
     */
    private Door passageDoor;
    /**
     *  The representation of the maximum number of sections to generate before you force a door.
     */
    private static final int ENDWHILE = 11;
    /**
     *  The only constructor for Passage.
     *  Creates and links anywhere between 1 and 10 passagesections, storing their doors in a hashmap.
     */
    public Passage() {
        thePassage = new ArrayList<PassageSection>();
        passageDoors = new ArrayList<Door>();
        passageDoor = null;
    }
    /**
     *  The builder called by the Passage constructor.
     *  Does the actual generation looping, creating monsters, stairs, and doors as directed by the table.
     */
    public void buildPassage() {
        D20 d20Roller = new D20();
        int sectionCounter = 0; //This adds up by one every time we add a section until 10 is hit
        while (sectionCounter < ENDWHILE) { //generate whole passage first, then find any doors
            if (sectionCounter == ENDWHILE - 1) {
                sectionCounter += setSection(1, sectionCounter); //Force creation of a door
            } else {
                sectionCounter += setSection(d20Roller.roll(), sectionCounter);
            }
            sectionCounter++;
        }
    }
    /**
     *  Adds a single passagesection to the array of sections.
     *  @param toAdd is the section to be added to the ArrayList.
     */
    public void addPassageSection(PassageSection toAdd) {
        thePassage.add(toAdd);
        if (toAdd.getDoor() != null) {
            passageDoors.add(toAdd.getDoor());
        }
    }
    /**
     *  Returns the arraylist of passage sections.
     *  @return ArrayList<PassageSection> is the full list.
     */
    public ArrayList<PassageSection> getPassageSections() {
        return thePassage;
    }
    /**
     *  Adds a single monster to the array of monsters.
     *  @param theMonster is the monster to be added to the ArrayList.
     *  @param i is the passagesection the monster is from.
     */
    private void addMonster(Monster theMonster, int i) {
        thePassage.get(i).setMonster(theMonster);
    }
    /**
     *  Returns a specified monster.
     *  @param i i is the section to get the monster from.
     *  @return Monster.
     */
    private Monster getMonster(int i) {
        PassageSection tempSection = thePassage.get(i);
        Monster returnMonster = tempSection.getMonster();
        return returnMonster;
    }
    /**
     *  Attempts to find an unused door.
     *  If it finds one, it creates a new chamber through the door
     *  And sets it to active.
     *  @return int that corresponds to the location of the door in the array of exits.
     */
    @Override
    public int setExitDoor() { //looks for a passagesection without a set door - returns the equivilant int in the array, and creates a chamber at that location
        for (int i = 0; i < passageDoors.size(); i++) {
            if (!passageDoors.get(i).getActive()) { //look for the first inactive door
                Chamber newChamber = new Chamber();
                passageDoors.get(i).setSpaces(this, newChamber);
                return i;
            }
        }
        return -1;
    }
    /**
     *  Sets an individual section of passage.
     *  @param rollIndex is the value to be used for the passage section generation
     *  @param sectionCounter is the value of the section's index to be added
     *  @return 0 if continues, 10 if dead end
     */
    public int setSection(int rollIndex, int sectionCounter) {
        int returnValue = 0;
        if (rollIndex <= 2) {
            addPassageSection(new PassageSection("The passage continues straight for 10 feet.", sectionCounter + 1));
        } else if (rollIndex >= 3 && rollIndex <= 5) {
            addPassageSection(new PassageSection("The passage ends in a door to a chamber.", sectionCounter + 1));
            returnValue = ENDWHILE;
        } else if (rollIndex >= 6 && rollIndex <= 7) {
            addPassageSection(new PassageSection("The passage continues straight for 10 feet. There is an archway to the right.", sectionCounter + 1));
        } else if (rollIndex >= 8 && rollIndex <= 9) {
            addPassageSection(new PassageSection("The passage continues straight for 10 feet. There is an archway to the left.", sectionCounter + 1));
        } else if (rollIndex >= 10 && rollIndex <= 11) {
            addPassageSection(new PassageSection("The passage veers to the left and continues for 10 feet.", sectionCounter + 1));
        } else if (rollIndex >= 12 && rollIndex <= 13) {
            addPassageSection(new PassageSection("The passage veers to the right and continues for 10 feet.", sectionCounter + 1));
        } else if (rollIndex >= 14 && rollIndex <= 16) {
            addPassageSection(new PassageSection("The passage ends in an archway to a chamber.", sectionCounter + 1));
            returnValue = ENDWHILE;
        } else if (rollIndex == 17) {
            addPassageSection(new PassageSection("The passage continues straight for 10 feet. There is a stairwell to the left.", sectionCounter + 1));
        } else if (rollIndex >= 18 && rollIndex <= 19) {
            addPassageSection(new PassageSection("The passage ends in a dead end.", sectionCounter + 1));
            returnValue = ENDWHILE;
        } else if (rollIndex == 20) {
            addPassageSection(new PassageSection("The passage continues straight for 10 feet... There is a monster in the path!", sectionCounter + 1));
        }
        return returnValue;
    }
    /**
     *  Adds all doors associated with a given passage section array to master array.
     *  @param passageList is the passages to check and add.
     */
    private void addDoors(ArrayList<PassageSection> passageList) {
        for (PassageSection p: passageList) {
            if (p.getDoor() != null) {
                passageDoors.add(p.getDoor());
            }
        }
    }
    /**
     *  Returns a single door corresponding to a particular passage.
     *  @param passageIndex passageIndex is the corresponding section where the door is located.
     *  @return Door at given section.
     */
    @Override
    public Door getDoor(int passageIndex) {
        if (passageIndex == -1) {
            return passageDoor;
        }
        if (passageIndex < passageDoors.size()) {
            return passageDoors.get(passageIndex);
        }
        return null;
    }
    /**
     *  This method returns the number of doors (excluding entrance door).
     *  @return An int representation of the number of doors.
     */
    @Override
    public int getNumDoors() {
        return passageDoors.size();
    }
    /**
     *  Sets an entrance door in the hashmap.
     *  Also has some code specifically made for the junit test that goes unused in the program.
     *  @param newDoor is the door to be set.
     */
    @Override
    public void setDoor(Door newDoor) {
        if (passageDoor == null) {
            passageDoor = newDoor;
        }
    }
    /**
     *  getMonsterDescription is a string creation helper.
     *  It uses the values in the sectionCheck to return a description.
     *  @param sectionCheck is the section to be checked.
     *  @return a string representation of the monster.
     */
    private String getMonsterDescription(PassageSection sectionCheck) {
        String returnDescription = new String();
        if (sectionCheck.getMonster() != null) {
            returnDescription = returnDescription.concat(" The monster is a(n) " + sectionCheck.getMonster().getDescription() + ".");
        }
        return returnDescription;
    }
    /**
     *  getStairsDescription is a string creation helper.
     *  It uses the values in the sectionCheck to return a description.
     *  @param sectionCheck is the section to be checked.
     *  @return a string representation of the stairs.
     */
    private String getStairsDescription(PassageSection sectionCheck) {
        String returnDescription = new String();
        if (sectionCheck.getStairs() != null) {
            returnDescription = returnDescription.concat(" The stairs go " + sectionCheck.getStairs().getDescription() + ".");
        }
        return returnDescription;
    }
    /**
     *  Returns the full description of the passage.
     *  @return String equal to the full set of all strings of all sections.
     */
    @Override
    public String getDescription() {
        String returnDescription = new String();
        returnDescription = "The passage is as follows:";
        for (int i = 0; i < thePassage.size(); i++) {
            returnDescription = returnDescription.concat("\n" + (i + 1) + " - " + thePassage.get(i).getDescription());
            returnDescription = returnDescription.concat(getMonsterDescription(thePassage.get(i)));
            returnDescription = returnDescription.concat(getStairsDescription(thePassage.get(i)));
        }
        returnDescription = returnDescription.concat("\n"); //add a newline for pictures
        return returnDescription;
    }
}
