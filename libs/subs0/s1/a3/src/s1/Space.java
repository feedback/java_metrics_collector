package s1;
/**
 * This file contains the abstract class Space,
 *  Which is used in the chamber and passage classes.
 * @author s1
 * @version 1.1
 * @since 2019-10-03
 */
public abstract class Space {
    /**
     *  This extendable method gets the description of a space.
     *  @return A string representation of the space.
     */
    public abstract String getDescription();
    /**
     *  This extendable method sets an entrance door.
     *  @param theDoor A door to be set as the entrance door.
     */
    public abstract void setDoor(Door theDoor);
    /**
     *  This extendable method gets an exit door.
     *  @param passageIndex is the index of the passage or exit to take the door from.
     *  @return A list of doors to be set.
     */
    public abstract Door getDoor(int passageIndex);
    /**
     *  This extendable method returns the number of doors (excluding entrance door).
     *  @return An int representation of the number of doors.
     */
    public abstract int getNumDoors();
    /**
     *  This extendable method sets a new active door, and returns its location.
     *  @return An integer location of the chosen door.
     */
    public abstract int setExitDoor();
}
