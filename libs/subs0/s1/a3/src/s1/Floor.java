package s1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
/**
 *  This class contains a class for the entire floor, which keeps track of all generated spaces.
 *  Once it's internal flags have been set (five rooms), it returns a floor - until then, it continuously regenerates.
 *  @author s1
 *  @version 1.2
 *  @since 2019-10-05
 */
public class Floor {
    /**
     *  A variable that stores the initial area, so we can get back to the start of the dungeon.
     */
    private Passage startingArea;
    /**
     *  An arraylist of chambers.
     */
    private ArrayList<Chamber> allChambers;
    /**
     *  The ratio of doors to chambers.
     */
    private HashMap<Door, ArrayList<Chamber>> doorConnections;
    /**
     *  A hashmap containing a door key, as well as its associated arraylist of chambers.
     */
    private int generateMode;
    /**
     *  The variable repesenting the correct max number of chambers.
     */
    private static final int MAXCHAMBERS = 5;
    /**
     *  The only constructor for Floor. Loops the floor creation until it meets the requirements.
     *  @param option denotes which algorithm to use.
     */
    public Floor(int option) {
        generateMode = option;
        if (option > 0) {
            int success = -1;
            while (success != MAXCHAMBERS) {
                startingArea = new Passage(); //Start in a passage
                startingArea.buildPassage();
                Door startingDoor = new Door();
                startingDoor.setSpaces(null, startingArea);
                Space tempSpace = startingArea;
                success = createFloor(tempSpace);
            }
        } else {
            allChambers = new ArrayList<Chamber>();
            doorConnections = new HashMap<Door, ArrayList<Chamber>>();
            createFloorNew();
        }
    }
    /**
     *  The floor creator. Continuously traverses and builds rooms alternating between passage and chamber until five chambers are built.
     *  @param tempSpace is the initial space to work with
     *  @return An integer representing the number of chambers that were generated.
     */
    private int createFloor(Space tempSpace) {
        int chamberCounter = 0;
        HashSet<Space> chamberSet = new HashSet<Space>(); //to see if we've already generated this room
        while (chamberCounter < MAXCHAMBERS) {
            int emptyDoorCheck = tempSpace.setExitDoor();
            Door tempDoor = tempSpace.getDoor(emptyDoorCheck);
            if (emptyDoorCheck > -1) { //we're dealing with an exit door to the space - in that case, space 0 is the current room, and space 1 is the next room
                tempSpace = tempDoor.getSpaces().get(1);
                if (tempSpace instanceof Chamber && !chamberSet.contains(tempSpace)) {
                    chamberSet.add(tempSpace);
                    chamberCounter++;
                }
            } else { // we're dealing with the entrance door to the space - in that case, space 0 is the previous room, and space 1 is the current room
                if (tempDoor.getSpaces().get(0) == null) {
                    break; //we're back at the entrance, nothing generated
                }
                tempSpace = tempDoor.getSpaces().get(0);
            }
        }
        return chamberCounter;
    }
    /**
     *  The new algorithm for assignment 3.
     */
    private void createFloorNew() {
        allChambers = createChamberList();
        for (int i = 0; i < 5; i++) {
            Chamber tempChamber = allChambers.get(i);
            for (int j = 0; j < tempChamber.getNumDoors(); j++) {
                Door tempStartDoor = tempChamber.getDoor(j);
                int chamberToTarget = i;
                Random rand = new Random();
                while (chamberToTarget == i) {
                    chamberToTarget =  rand.nextInt(5);
                }
                ArrayList<Chamber> tempChamberList = new ArrayList<Chamber>();
                tempChamberList.add(tempChamber);
                tempChamberList.add(allChambers.get(chamberToTarget));
                Door tempEndDoor = null;
                for (int k = 0; k < allChambers.get(chamberToTarget).getNumDoors(); k++) {
                    if (!allChambers.get(chamberToTarget).getDoor(k).getActive()) {
                        tempEndDoor = allChambers.get(chamberToTarget).getDoor(k);
                    }
                }
                if (tempEndDoor == null) {
                    tempEndDoor = allChambers.get(chamberToTarget).getDoor(0);
                }
                addDoorPassage(tempStartDoor, tempEndDoor, tempChamber, allChambers.get(chamberToTarget));
                doorConnections.put(tempStartDoor, tempChamberList);
                doorConnections.put(tempEndDoor, tempChamberList);
            }
        }
    }
    /**
     *  A method that creates a list of chambers for use in the new algorithm.
     *  @return An arrayList of chambers
     */
    private ArrayList<Chamber> createChamberList() {
        ArrayList<Chamber> tempList = new ArrayList<Chamber>();
        for (int i = 0; i < 5; i++) {
            Chamber tempChamber = new Chamber();
            tempList.add(tempChamber);
        }
        return tempList;
    }
    /**
     *  A method used by the new algorthm to set the doors according to specification.
     *  @param givenStartDoor is the door at the first end of the passage.
     *  @param givenEndDoor is the door at the other end of the passage.
     *  @param startChamber is the first connectiong chamber.
     *  @param endChamber is the second connecting chamber.
     */
    private void addDoorPassage(Door givenStartDoor, Door givenEndDoor, Chamber startChamber, Chamber endChamber) {
        Passage addedPassage = new Passage();
        PassageSection section1 = new PassageSection("The passage continues straight for 10 feet. There is an archway to the left.", 0);
        PassageSection section2 = new PassageSection("The passage continues straight for 10 feet. There is an archway to the left.", 1);
        section1.setDoor(givenStartDoor, true);
        section2.setDoor(givenEndDoor, true);
        addedPassage.addPassageSection(section1);
        addedPassage.addPassageSection(section2);
        givenStartDoor.setSpaces(startChamber, addedPassage);
        givenEndDoor.setSpaces(endChamber, addedPassage);
    }
    /**
     *  A getter that returns the starting area.
     *  @return A space that is at the start of the dungeon.
     */
    public Space getStartingSpace() {
        return startingArea;
    }
    /**
     *  A helper function used to generate the description for the simple algorithm.
     *  @return A string representation of the dungeon.
     */
    public String simpleDescription() {
        String returnDescription = new String();
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < allChambers.get(i).getNumDoors(); j++) {
                returnDescription = returnDescription.concat("******************************\n*       C H A M B E R        *\n******************************\n" + allChambers.get(i).getDescription());
                if (!allChambers.get(i).getDoor(j).getSeen()) {
                    allChambers.get(i).getDoor(j).setSeen(true);
                    returnDescription = returnDescription.concat("\nTraversing through door in exit " + (j + 1) + "\n******************************\n*       P A S S A G E        *\n******************************\n" + allChambers.get(i).getDoor(j).getSpaces().get(1).getDescription());
                }
            }
        }
        return returnDescription;
    }
    /**
     *  A helper function used to generate the description for the complex algorithm.
     *  @param tempSpace is the starting space from which to expend outward.
     *  @return A string representation of the dungeon.
     */
    public String complexDescription(Space tempSpace) {
        String returnDescription = new String();
        int chamberCounter = 0; //Go until this hits 5 - the number of chambers we want
        HashSet<Space> chamberSet = new HashSet<Space>(); //to see if we've already generated this room
        while (chamberCounter < MAXCHAMBERS) {
            if (tempSpace instanceof Chamber) {
                returnDescription = returnDescription.concat("******************************\n*       C H A M B E R        *\n******************************\n");
                if (!chamberSet.contains(tempSpace)) {
                    chamberSet.add(tempSpace);
                    chamberCounter++;
                }
            } else {
                returnDescription = returnDescription.concat("******************************\n*       P A S S A G E        *\n******************************\n");
            }
            returnDescription = returnDescription.concat(tempSpace.getDescription());
            boolean doorExists = false;
            for (int i = 0; i < tempSpace.getNumDoors(); i++) {
                Door tempDoor = tempSpace.getDoor(i);
                if (tempDoor != null && tempDoor.getActive() && !tempDoor.getSeen()) {
                    tempDoor.setSeen(true);
                    returnDescription = returnDescription.concat("\n" + tempDoor.getDescription() + "\n\n");
                    doorExists = true;
                    tempSpace = tempDoor.getSpaces().get(1);
                    break;
                }
            }
            if (!doorExists && chamberCounter < 5) {
                returnDescription = returnDescription.concat("\nThis space is a dead end. Returning to previous space.\n\n");
                tempSpace = tempSpace.getDoor(-1).getSpaces().get(0);
            }
        }
        return returnDescription;
    }
    /**
     *  A description generator that concatanates all the space descriptions.
     *  @return The entire string representing the whole dungeon.
     */
    public String getDescription() {
        String returnDescription = new String();
        returnDescription = returnDescription.concat("******************************\n* D U N G E O N    S T A R T *\n******************************\n");
        if (generateMode == 0) {
            returnDescription = returnDescription.concat(simpleDescription());
        } else {
            returnDescription = returnDescription.concat(complexDescription(startingArea));
        }
        returnDescription = returnDescription.concat("******************************\n*    D U N G E O N   E N D   *\n******************************\n");
        return returnDescription;
    }
}
