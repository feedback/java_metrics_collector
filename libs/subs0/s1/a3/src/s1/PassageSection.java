package s1;
import dnd.die.D20;
import dnd.die.Percentile;
import dnd.models.Monster;
import dnd.models.Stairs;
/**
 * This is the passagesection class of the ajetleb package. Utility is to set any individual passagesection for the whole passage.
 * @author s1
 * @version 1.2
 * @since 2019-09-30
 */
public class PassageSection {
    /**
     *  Stores a single monster possibly located in the section.
     */
    private Monster passageMonster;
    /**
     *  Stores a single door possibly located in the section.
     */
    private Door passageDoor;
    /**
     *  Stores a single stairs possible located in the section.
     */
    private Stairs passageStairs;
    /**
     *  Stores the passage # of the current section.
     */
    private int sectionNumber;
    /**
     *  Stores a string for the contents of the section.
     */
    private String setDescription;
    /**
     *  A default constructor that makes the most basic type of passage.
     */
    public PassageSection() {
        passageMonster = null;
        passageDoor = null;
        passageStairs = null;
        sectionNumber = -1;
        setDescription = "The passage continues straight for 10 feet.";
    }
    /**
     *  A constructor that builds the chamber based on the string from the table given.
     *  @param description description is the table string value.
     *  @param secNum is the section number of the passagesection
     */
    public PassageSection(String description, int secNum) {
        this(); //Call default constructor first, then change
        setDescription = description;
        setSection(secNum);
        if (setDescription.contains("door") || setDescription.contains("Door")) {
            setDoor(new Door(), false);
        }
        if (setDescription.contains("archway") || setDescription.contains("Archway")) {
            setDoor(new Door(), true);
        }
        if (setDescription.contains("stairwell") || setDescription.contains("Stairwell")) {
            setStairs(new Stairs());
        }
        if (setDescription.contains("monster") || setDescription.contains("Monster")) {
            setMonster(new Monster());
        }
    }
    /**
     *  Sets the door.
     *  @param setDoor is the door to set.
     *  @param isArchway is the boolean representation of the archway state.
     */
    public void setDoor(Door setDoor, boolean isArchway) {
        passageDoor = setDoor;
        if (isArchway) {
            passageDoor.setArchway(0);
        } else {
            passageDoor.setArchway(5);
        }
        passageDoor.setPassage(getSection());
    }
    /**
     *  Returns a door stored (can be null).
     *  @return A door.
     */
    public Door getDoor() {
        return passageDoor;
    }
    /**
     *  Sets the monster.
     *  @param setMonster is the door to set.
     */
    public void setMonster(Monster setMonster) {
        Percentile d100 = new Percentile();
        setMonster.setType(d100.roll());
        passageMonster = setMonster;
    }
    /**
     *  Returns a Monster stored (can be null).
     *  @return A Monster.
     */
    public Monster getMonster() {
        return passageMonster;
    }
    /**
     *  Sets the Stairs.
     *  @param setStairs is the door to set.
     */
    public void setStairs(Stairs setStairs) {
        D20 d20 = new D20();
        setStairs.setType(d20.roll());
        passageStairs = setStairs;
    }
    /**
     *  Returns a Stairs stored (can be null).
     *  @return A Stairs.
     */
    public Stairs getStairs() {
        return passageStairs;
    }
    /**
     *  Sets the section number.
     *  @param setNum is the number to set.
     */
    public void setSection(int setNum) {
        sectionNumber = setNum;
    }
    /**
     *  Returns the section number.
     *  @return A section number.
     */
    public int getSection() {
        return sectionNumber;
    }
    /**
     *  getDescription compiles a string description of the singular section.
     *  @return A string representation of the room.
     */
    public String getDescription() {
        return setDescription;
    }
}
