package s1;
import dnd.die.D20;
import dnd.die.D6;
import dnd.models.Exit;
import dnd.models.Trap;

import java.util.ArrayList;
/**
 * This is the door class of the ajetleb package. Utility is to get and set door space.
 * @author s1
 * @version 1.3
 * @since 2019-09-30
 */
public class Door {
    /**
     *  A boolean identifying if the door is open.
     */
    private boolean isOpen;
    /**
     *  A boolean identifying if the door is an archway.
     */
    private boolean isArchway;
    /**
     *  A boolean identifying if the door is trapped.
     */
    private boolean isTrapped;
    /**
     *  A boolean identifying if the door is locked.
     */
    private boolean isLocked;
    /**
     *  A boolean identifying if the door is active.
     */
    private boolean isDoor;
    /**
     *  A boolean identifying if the door has been seen by the description getter.
     */
    private boolean isSeen;
    /**
     *  A trap possibly stored by the door.
     */
    private Trap doorTrap;
    /**
     *  An arraylist of the two connected spaces.
     */
    private ArrayList<Space> storedSpaces;
    /**
     *  An associated possible exit.
     */
    private Exit storedExit;
    /**
     *  An integer containing the section or exit number a door is stored in.
     */
    private int sectionNum;
    /**
     *  A basic constructor for the door.
     *  Randomly generates the door according to a provided table.
     */
    public Door() {
        sectionNum = -1;
        isDoor = false;
        isSeen = false;
        isOpen = false;
        isArchway = false;
        isTrapped = false;
        isLocked = false;
        storedSpaces = new ArrayList<Space>();
        setStates();
    }
    /**
     *  A setter that decides the values for the door.
     */
    private void setStates() {
        D20 detailRoller = new D20();
        setArchway(detailRoller.roll());
        setTrapped(detailRoller.roll());
        D6 secondRoller = new D6();
        setLocked(secondRoller.roll());
        setOpen(secondRoller.roll());
    }
    /**
     *  A setter to set the door as archway or not.
     *  Changes other booleans depending on result.
     *  @param roll is a roll to see whether it will be an archway.
     */
    public void setArchway(int roll) {
        if (roll <= 2) {
            isArchway = true;
            isOpen = true;
            isTrapped = false;
            isLocked = false;
        } else {
            isArchway = false;
        }
    }
    /**
     *  A return based on the state of isArchway.
     *  @return A boolean representation of isArchway.
     */
    public boolean getArchway() {
        return isArchway;
    }
    /**
     *  A setter to set the door as trapped.
     *  Only sets if the door isnt an archway.
     *  @param roll Is a roll to see whether it will be trapped.
     */
    public void setTrapped(int roll) {
        if (!isArchway && roll == 1) {
            isTrapped = true;
            doorTrap = new Trap();
            D20 chooser = new D20();
            doorTrap.chooseTrap(chooser.roll());
        } else {
            isTrapped = false;
        }
    }
    /**
     *  A return based on the state of isTrapped.
     *  @return A boolean representation of isTrapped.
     */
    public boolean getTrapped() {
        return isTrapped;
    }
    /**
     *  A seeter to set the door as locked or not.
     *  Only sets as locked if the door isn't an archway and is closed.
     *  @param roll Is a roll to see whether it will be locked.
     */
    public void setLocked(int roll) {
        if (!isArchway && !isOpen && roll == 1) {
            isLocked = true;
        } else {
            isLocked = false;
        }
    }
    /**
     *  A return based on the state of isLocked.
     *  @return  A boolean representation of isLocked.
     */
    public boolean getLocked() {
        return isLocked;
    }
    /**
     *  A setter to set the door as open or closed.
     *  Only sets closed if the door isnt an archway.
     *  @param roll Is a roll to see whether it will be open.
     */
    public void setOpen(int roll) {
        if ((!isLocked && roll <= 3) || isArchway)  {
            isOpen = true;
        } else {
            isOpen = false;
        }
    }
    /**
     *  A return based on the state of isOpen.
     *  @return A boolean representation of isOpen.
     */
    public boolean getOpen() {
        return isOpen;
    }
    /**
     *  A setter that sets the door active or not.
     *  This is used to determine if the door has already been traversed.
     *  @param flag Is true or false depending on how you want to set active.
     */
    public void setActive(boolean flag) {
        isDoor = flag;
    }
    /**
     *  A return based on the state of isDoor.
     *  @return A boolean representation of isDoor.
     */
    public boolean getActive() {
        return isDoor;
    }
    /**
     *  A setter that sets the door seen or not.
     *  This is used to determine if the door has already been traversed in description obtaining.
     *  @param flag Is true or false depending on how you want to set seen.
     */
    public void setSeen(boolean flag) {
        isSeen = flag;
    }
    /**
     *  A return based on the state of isSeen.
     *  @return A boolean representation of isSeen.
     */
    public boolean getSeen() {
        return isSeen;
    }
    /**
     *  Sets the passagesection that the door is located in.
     *  @param sectionInt is the value to be set.
     */
    public void setPassage(int sectionInt) {
        sectionNum = sectionInt;
    }
    /**
     *  Gets the passagesection that the door is located in.
     *  @return The integer value of the location of the door.
     */
    public int getPassage() {
        return sectionNum;
    }
    /**
     *  Sets the two spaces the door connects.
     *  @param spaceOne is the first space (that the door comes from).
     *  @param spaceTwo is the second space (that the door leads to).
     */
    public void setSpaces(Space spaceOne, Space spaceTwo) {
        addSpace(spaceOne);
        addSpace(spaceTwo);
        setActive(true);
    }
    /**
     *  Returns the associated spaces.
     *  @return An arraylist of 2 spaces.
     */
    public ArrayList<Space> getSpaces() {
        return storedSpaces;
    }
    /**
     *  Does the dirty work for the setSpaces class.
     *  Adds the given space to the array of spaces, and sets its door as this door.
     *  @param toBeAdded is the space to be added to the array.
     */
    private void addSpace(Space toBeAdded) {
        storedSpaces.add(toBeAdded);
        if (toBeAdded != null) {
            toBeAdded.setDoor(this);
        }
    }
    /**
     *  Returns the description of the archway.
     *  @return A string representation of the stored archway.
     */
    private String getArchwayDescription() {
        String returnString = new String();
        if (isArchway) {
            returnString = returnString.concat("The connector is an archway. ");
        } else {
            returnString = returnString.concat("The connector is a door. ");
        }
        return returnString;
    }
    /**
     *  Returns the description of the lockstate.
     *  @return A string representation of the stored lockstate.
     */
    private String getLockedDescription() {
        String returnString = new String();
        if (isLocked) {
            returnString = returnString.concat("It is locked. ");
        } else {
            returnString = returnString.concat("It is not locked. ");
        }
        return returnString;
    }
    /**
     *  Returns the description of the openstate.
     *  @return A string representation of the stored openstate.
     */
    private String getOpenDescription() {
        String returnString = new String();
        if (isOpen) {
            returnString = returnString.concat("It is open.");
        } else {
            returnString = returnString.concat("It is closed.");
        }
        return returnString;
    }
    /**
     *  Returns the description of the trap.
     *  @return A string representation of the stored trap.
     */
    private String getTrapDescription() {
        String returnString = new String();
        if (isTrapped) {
            returnString = ("It is trapped with " + doorTrap.getDescription());
        } else {
            returnString = "It is not trapped. ";
        }
        return returnString;
    }
    /**
     *  Returns the description of the door.
     *  @return A string representation of the stored door.
     */
    public String getDescription() {
        String doorDescription = new String();
        doorDescription = doorDescription.concat("Passing through connector in exit " + getPassage() + ".\n");
        doorDescription = doorDescription.concat(getArchwayDescription());
        doorDescription = doorDescription.concat(getTrapDescription());
        doorDescription = doorDescription.concat(getLockedDescription());
        doorDescription = doorDescription.concat(getOpenDescription());
        return doorDescription;
    }
}
