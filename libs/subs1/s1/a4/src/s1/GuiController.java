package s1;

import dnd.models.DnDElement;
import dnd.models.Monster;
import dnd.models.Treasure;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

class GuiController {
    /**
     *  The currently loaded floor.
     */
    private Floor genFloor;
    private Space genSpace;
    GuiController() {
        genFloor = new Floor();
        genFloor.generateFloor();
    }
    void setFloor(Floor givenFloor) {
        genFloor = givenFloor;
    }
    ArrayList<LevelComponent> getSpaces() {
        return genFloor.getSpacesList();
    }
    void saveFloor(ObjectOutputStream objToFile) {
        try {
            objToFile.writeObject(genFloor);
        } catch (IOException e) {
            System.out.println("Unable to print");
        }
    }
    void regenerateFloor() {
        genFloor.generateFloor();
    }
    ArrayList<Monster> getMonsters(Space givenSpace) {
        return givenSpace.getMonsters();
    }
    ArrayList<Treasure> getTreasures(Space givenSpace) {
        return givenSpace.getTreasures();
    }
    void setSpace(Space givenSpace) {
        genSpace = givenSpace;
    }
    Space getSpace() {
        return genSpace;
    }
    void addTreasure(String chosenOption) {
        Treasure tempTreasure = new Treasure();
        tempTreasure.chooseTreasure(0);
        int i = 0;
        while (!tempTreasure.getDescription().contains(chosenOption) &&  i < 100) {
            tempTreasure.chooseTreasure(i);
            i++;
        }
        i = 0;
        if (getSpace() instanceof Passage) {
            Random choosePassage = new Random();
            i = choosePassage.nextInt(((Passage) getSpace()).getPassageSections().size());
        } else {
            if (getSpace().getMonsters().size() > 0) {
                ((Chamber) getSpace()).setContents(15);
            } else {
                ((Chamber) getSpace()).setContents(20);
            }
        }
        getSpace().addTreasure(tempTreasure, i);
    }
    void addMonster(String chosenOption) {
        Monster tempMonster = new Monster();
        int i = 1;
        while (!tempMonster.getDescription().equals(chosenOption) && i < 101) {
            tempMonster.setType(i);
            i++;
        }
        i = 0;
        if (getSpace() instanceof Passage) {
            Random choosePassage = new Random();
            i = choosePassage.nextInt(((Passage) getSpace()).getPassageSections().size());
        } else {
            if (getSpace().getTreasures().size() > 0) {
                ((Chamber) getSpace()).setContents(15);
            } else {
                ((Chamber) getSpace()).setContents(13);
            }
        }
        getSpace().addMonster(tempMonster, i);
    }
    void removeElement(DnDElement removedElement) {
        if (removedElement instanceof Monster) {
            getSpace().removeMonster((Monster) removedElement);
            if (getSpace() instanceof Chamber) {
                if (getSpace().getMonsters().size() == 0) {
                    if (((Chamber) getSpace()).getContents().contains("treasure")) {
                        ((Chamber) getSpace()).setContents(20);
                    } else {
                        ((Chamber) getSpace()).setContents(1);
                    }
                }
            }
        } else {
            getSpace().removeTreasure((Treasure) removedElement);
            if (getSpace() instanceof Chamber) {
                if (getSpace().getTreasures().size() == 0) {
                    if (((Chamber) getSpace()).getContents().contains("monster")) {
                        ((Chamber) getSpace()).setContents(14);
                    } else {
                        ((Chamber) getSpace()).setContents(1);
                    }
                }
            }
        }
    }
}
