package s1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *  This class contains a class for the entire floor, which keeps track of all generated spaces.
 *  Once it's internal flags have been set (five rooms), it returns a floor - until then, it continuously regenerates.
 *  @author s1
 *  @version 1.2
 *  @since 2019-10-05
 */
public class Floor implements Serializable {
    /**
     *  A variable that stores the initial area, so we can get back to the start of the dungeon.
     */
    private Passage startingArea;
    /**
     *  The variable representing the correct max number of chambers.
     */
    private static final int MAXCHAMBERS = 5;
    /**
     *  An arrayList of spaces.
     */
    private ArrayList<LevelComponent> allSpaces;
    /**
     *  The only constructor for Floor. Loops the floor creation until it meets the requirements.
     */
    Floor() {
        allSpaces = new ArrayList<>();
    }
    /**
     * Generates the floor.
     */
    void generateFloor() {
        allSpaces.clear();
        int success = -1;
        while (success != MAXCHAMBERS) {
            startingArea = new Passage(); //Start in a passage
            startingArea.buildPassage();
            Door startingDoor = new Door();
            startingDoor.setSpaces(null, startingArea);
            Space tempSpace = startingArea;
            success = createFloor(tempSpace);
        }
    }
    /**
     *  The floor creator. Continuously traverses and builds rooms alternating between passage and chamber until five chambers are built.
     *  @param tempSpace is the initial space to work with
     *  @return An integer representing the number of chambers that were generated.
     */
    private int createFloor(Space tempSpace) {
        int chamberCounter = 0;
        int passageCounter = 0;
        while (chamberCounter < MAXCHAMBERS) {
            int emptyDoorCheck = tempSpace.setExitDoor();
            Door tempDoor = tempSpace.getDoor(emptyDoorCheck);
            if (emptyDoorCheck > -1) { //we're dealing with an exit door to the space - in that case, space 0 is the current room, and space 1 is the next room
                tempSpace = tempDoor.getSpaces().get(1);
                if (!allSpaces.contains(tempSpace)) {
                    if (tempSpace instanceof Chamber) {
                        chamberCounter++;
                        tempSpace.setId("Chamber " + chamberCounter);
                    } else {
                        passageCounter++;
                        tempSpace.setId("Passage " + passageCounter);
                    }
                    allSpaces.add(tempSpace);
                }
            } else { // we're dealing with the entrance door to the space - in that case, space 0 is the previous room, and space 1 is the current room
                if (tempDoor.getSpaces().get(0) == null) {
                    break; //we're back at the entrance, nothing generated
                }
                tempSpace = tempDoor.getSpaces().get(0);
            }
        }
        return chamberCounter;
    }
    /**
     * A method that returns all the spaces that can be found.
     * @return An array list of spaces
     */
    ArrayList<LevelComponent> getSpacesList() {
        return allSpaces;
    }
    /**
     *  A helper function used to generate the description for the complex algorithm.
     *  @param tempSpace is the starting space from which to expend outward.
     *  @return A string representation of the dungeon.
     */
    private String complexDescription(Space tempSpace) {
        String returnDescription = "";
        int chamberCounter = 0; //Go until this hits 5 - the number of chambers we want
        HashSet<Space> chamberSet = new HashSet<>(); //to see if we've already generated this room
        while (chamberCounter < MAXCHAMBERS) {
            if (tempSpace instanceof Chamber) {
                returnDescription = returnDescription.concat("******************************\n*       C H A M B E R        *\n******************************\n");
                if (!chamberSet.contains(tempSpace)) {
                    chamberSet.add(tempSpace);
                    chamberCounter++;
                }
            } else {
                returnDescription = returnDescription.concat("******************************\n*       P A S S A G E        *\n******************************\n");
            }
            returnDescription = returnDescription.concat(tempSpace.getDescription());
            boolean doorExists = false;
            for (int i = 0; i < tempSpace.getNumDoors(); i++) {
                Door tempDoor = tempSpace.getDoor(i);
                if (tempDoor != null && tempDoor.getActive() && !tempDoor.getSeen()) {
                    tempDoor.setSeen(true);
                    returnDescription = returnDescription.concat("\n" + tempDoor.getDescription() + "\n\n");
                    doorExists = true;
                    tempSpace = tempDoor.getSpaces().get(1);
                    break;
                }
            }
            if (!doorExists && chamberCounter < 5) {
                returnDescription = returnDescription.concat("\nThis space is a dead end. Returning to previous space.\n\n");
                tempSpace = tempSpace.getDoor(-1).getSpaces().get(0);
            }
        }
        return returnDescription;
    }
    /**
     *  A description generator that concatenates all the space descriptions.
     *  @return The entire string representing the whole dungeon.
     */
    public String getDescription() {
        String returnDescription = "";
        returnDescription = returnDescription.concat("******************************\n* D U N G E O N    S T A R T *\n******************************\n");
        returnDescription = returnDescription.concat(complexDescription(startingArea));
        returnDescription = returnDescription.concat("******************************\n*    D U N G E O N   E N D   *\n******************************\n");
        return returnDescription;
    }
}
