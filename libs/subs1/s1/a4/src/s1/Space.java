package s1;

import dnd.models.Monster;
import dnd.models.Treasure;

import java.util.ArrayList;

/**
 * This file contains the abstract class Space,
 *  Which is used in the chamber and passage classes.
 * @author s1
 * @version 1.1
 * @since 2019-10-03
 */
public abstract class Space extends LevelComponent {
    /**
     *  This extendable method sets an entrance door.
     *  @param theDoor A door to be set as the entrance door.
     */
    public abstract void setDoor(Door theDoor);
    /**
     *  This extendable method gets an exit door.
     *  @param passageIndex is the index of the passage or exit to take the door from.
     *  @return A list of doors to be set.
     */
    public abstract Door getDoor(int passageIndex);
    /**
     *  This extendable method returns the number of doors (excluding entrance door).
     *  @return An int representation of the number of doors.
     */
    public abstract int getNumDoors();
    /**
     *  This extendable method sets a new active door, and returns its location.
     *  @return An integer location of the chosen door.
     */
    public abstract int setExitDoor();
    /**
     * This method returns an array of monsters.
     * @return getMonsters(), an array of monsters.
     */
    public abstract ArrayList<Monster> getMonsters();
    /**
     * This method returns an array of treasures.
     * @return getTreasures(), an array of treasures.
     */
    public abstract ArrayList<Treasure> getTreasures();
    /**
     * This method adds a monster to the array.
     * @param theMonster is the monster to add
     * @param i is the index to add the monster.
     */
    public abstract void addMonster(Monster theMonster, int... i);
    /**
     * This method adds a treasure to the array.
     * @param theTreasure is the monster to add
     * @param i is the index to add the monster.
     */
    public abstract void addTreasure(Treasure theTreasure, int... i);
    /**
     * This method removes a monster.
     * @param m is the monster to remove.
     */
    public abstract void removeMonster(Monster m);
    /**
     * This method removes a treasure.
     * @param t is the treasure to remove.
     */
    public abstract void removeTreasure(Treasure t);
    }
