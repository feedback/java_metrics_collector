package s1;

import dnd.die.D20;
import dnd.die.D6;
import dnd.models.Exit;
import dnd.models.Trap;

import java.util.ArrayList;
/**
 * This is the door class of the ajetleb package. Utility is to get and set door space.
 * @author s1
 * @version 1.3
 * @since 2019-09-30
 */
public class Door extends LevelComponent {
    /**
     *  A boolean identifying if the door is open.
     */
    private boolean isOpen;
    /**
     *  A boolean identifying if the door is an archway.
     */
    private boolean isArchway;
    /**
     *  A boolean identifying if the door is trapped.
     */
    private boolean isTrapped;
    /**
     *  A boolean identifying if the door is locked.
     */
    private boolean isLocked;
    /**
     *  A boolean identifying if the door is active.
     */
    private boolean isDoor;
    /**
     *  A boolean identifying if the door has been seen by the description getter.
     */
    private boolean isSeen;
    /**
     *  A trap possibly stored by the door.
     */
    private Trap doorTrap;
    /**
     *  An arraylist of the two connected spaces.
     */
    private ArrayList<Space> storedSpaces;
    /**
     *  An associated possible exit.
     */
    private Exit storedExit;
    /**
     *  An integer containing the section or exit number a door is stored in.
     */
    private int sectionNum;
    /**
     *  A basic constructor for the door.
     *  Randomly generates the door according to a provided table.
     */
    Door() {
        setId("Door");
        sectionNum = -1;
        isDoor = false;
        isSeen = false;
        isOpen = false;
        isArchway = false;
        isTrapped = false;
        isLocked = false;
        storedSpaces = new ArrayList<>();
        setStates();
    }
    /**
     * Returns this door.
     * @return An arraylist of doors (just this one).
     */
    @Override
    public ArrayList<LevelComponent> getDoors() {
        ArrayList<LevelComponent> doorList = new ArrayList<>();
        doorList.add(this);
        return doorList;
    }
    /**
     *  A setter that decides the values for the door.
     */
    private void setStates() {
        D20 detailRoller = new D20();
        setArchway(detailRoller.roll());
        setTrapped(detailRoller.roll());
        D6 secondRoller = new D6();
        setLocked(secondRoller.roll());
        setOpen(secondRoller.roll());
    }
    /**
     *  A setter to set the door as archway or not.
     *  Changes other booleans depending on result.
     *  @param roll is a roll to see whether it will be an archway.
     */
    void setArchway(int roll) {
        if (roll <= 2) {
            isArchway = true;
            isOpen = true;
            isTrapped = false;
            isLocked = false;
        } else {
            isArchway = false;
        }
    }
    /**
     *  A return based on the state of isArchway.
     *  @return A boolean representation of isArchway.
     */
    public boolean getArchway() {
        return isArchway;
    }
    /**
     *  A setter to set the door as trapped.
     *  Only sets if the door isnt an archway.
     *  @param roll Is a roll to see whether it will be trapped.
     */
    private void setTrapped(int roll) {
        if (!isArchway && roll == 1) {
            isTrapped = true;
            doorTrap = new Trap();
            D20 chooser = new D20();
            doorTrap.chooseTrap(chooser.roll());
        } else {
            isTrapped = false;
        }
    }
    /**
     *  A return based on the state of isTrapped.
     *  @return A boolean representation of isTrapped.
     */
    public boolean getTrapped() {
        return isTrapped;
    }
    /**
     *  A seeter to set the door as locked or not.
     *  Only sets as locked if the door isn't an archway and is closed.
     *  @param roll Is a roll to see whether it will be locked.
     */
    private void setLocked(int roll) {
        isLocked = !isArchway && !isOpen && roll == 1;
    }
    /**
     *  A return based on the state of isLocked.
     *  @return  A boolean representation of isLocked.
     */
    public boolean getLocked() {
        return isLocked;
    }
    /**
     *  A setter to set the door as open or closed.
     *  Only sets closed if the door isnt an archway.
     *  @param roll Is a roll to see whether it will be open.
     */
    private void setOpen(int roll) {
        isOpen = (!isLocked && roll <= 3) || isArchway;
    }
    /**
     *  A return based on the state of isOpen.
     *  @return A boolean representation of isOpen.
     */
    public boolean getOpen() {
        return isOpen;
    }
    /**
     *  A setter that sets the door active or not.
     *  This is used to determine if the door has already been traversed.
     *  @param flag Is true or false depending on how you want to set active.
     */
    private void setActive(boolean flag) {
        isDoor = flag;
    }
    /**
     *  A return based on the state of isDoor.
     *  @return A boolean representation of isDoor.
     */
    boolean getActive() {
        return isDoor;
    }
    /**
     *  A setter that sets the door seen or not.
     *  This is used to determine if the door has already been traversed in description obtaining.
     *  @param flag Is true or false depending on how you want to set seen.
     */
    void setSeen(boolean flag) {
        isSeen = flag;
    }
    /**
     *  A return based on the state of isSeen.
     *  @return A boolean representation of isSeen.
     */
    boolean getSeen() {
        return isSeen;
    }
    /**
     *  Sets the passagesection that the door is located in.
     *  @param sectionInt is the value to be set.
     */
    void setPassage(int sectionInt) {
        sectionNum = sectionInt;
    }
    /**
     *  Gets the passagesection that the door is located in.
     *  @return The integer value of the location of the door.
     */
    public int getPassage() {
        return sectionNum;
    }
    /**
     *  Sets the two spaces the door connects.
     *  @param spaceOne is the first space (that the door comes from).
     *  @param spaceTwo is the second space (that the door leads to).
     */
    void setSpaces(Space spaceOne, Space spaceTwo) {
        addSpace(spaceOne);
        addSpace(spaceTwo);
        setActive(true);
    }
    /**
     *  Returns the associated spaces.
     *  @return An arraylist of 2 spaces.
     */
    ArrayList<Space> getSpaces() {
        return storedSpaces;
    }
    /**
     *  Does the dirty work for the setSpaces class.
     *  Adds the given space to the array of spaces, and sets its door as this door.
     *  @param toBeAdded is the space to be added to the array.
     */
    private void addSpace(Space toBeAdded) {
        storedSpaces.add(toBeAdded);
        if (toBeAdded != null) {
            toBeAdded.setDoor(this);
        }
    }
    /**
     *  Returns the description of the archway.
     *  @return A string representation of the stored archway.
     */
    private String getArchwayDescription() {
        String returnString = "";
        if (isArchway) {
            returnString = returnString.concat("The connector is an archway.\n");
        } else {
            returnString = returnString.concat("The connector is a door.\n");
        }
        return returnString;
    }
    /**
     *  Returns the description of the lockstate.
     *  @return A string representation of the stored lockstate.
     */
    private String getLockedDescription() {
        String returnString = "";
        if (isLocked) {
            returnString = returnString.concat("It is locked.\n");
        } else {
            returnString = returnString.concat("It is not locked.\n");
        }
        return returnString;
    }
    /**
     *  Returns the description of the openstate.
     *  @return A string representation of the stored openstate.
     */
    private String getOpenDescription() {
        String returnString = "";
        if (isOpen) {
            returnString = returnString.concat("It is open.\n");
        } else {
            returnString = returnString.concat("It is closed.\n");
        }
        return returnString;
    }
    /**
     *  Returns the description of the connected spaces.
     *  @return A string representation of the stored spaces.
     */
    private String getConnectedDescription() {
        String returnString = "";
        for (Space i: storedSpaces) {
            if (i.getId() != null) {
                returnString = returnString.concat("It is connected to " + i.getId() + ".\n");
            }
        }
        return returnString;
    }
    /**
     *  Returns the description of the trap.
     *  @return A string representation of the stored trap.
     */
    private String getTrapDescription() {
        String returnString;
        if (isTrapped) {
            returnString = ("It is trapped with " + doorTrap.getDescription() + "\n");
        } else {
            returnString = "It is not trapped.\n";
        }
        return returnString;
    }
    /**
     *  Returns the description of the door.
     *  @return A string representation of the stored door.
     */
    @Override
    public String getDescription() {
        String doorDescription = "";
        doorDescription = doorDescription.concat(getArchwayDescription());
        doorDescription = doorDescription.concat(getTrapDescription());
        doorDescription = doorDescription.concat(getLockedDescription());
        doorDescription = doorDescription.concat(getOpenDescription());
        doorDescription = doorDescription.concat(getConnectedDescription());
        return doorDescription;
    }
}
