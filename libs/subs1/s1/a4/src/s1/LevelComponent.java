package s1;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class LevelComponent implements Serializable {
    /**
     *  This extendable method gets the description of a space.
     *  @return A string representation of the space.
     */
    public abstract String getDescription();
    private String id;
    /**
     *  The id for the given component.
     *  @return id, A string representation of the id.
     */
    String getId() {
        return id;
    }
    /**
     *  The id to be set for the given component.
     *  @param givenId is the id to be set.
     */
    void setId(String givenId) {
        id = givenId;
    }

    /**
     * An abstract method that gets all associated doors.
     * @return An arrayList of doors.
     */
    public abstract ArrayList<LevelComponent> getDoors();
}
