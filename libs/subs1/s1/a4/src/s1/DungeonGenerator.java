package s1;

import dnd.models.DnDElement;
import dnd.models.Monster;
import dnd.models.Treasure;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * This is the main class of the DND package. Based heavily on code used in the first assignment.
 * It simply signals other classes to run.
 *
 * @author s1
 * @version 1.1
 * @since 2019-09-30
 */
public class DungeonGenerator extends Application {
    private GuiController stageController;
    private Stage primaryStage; //The main stage
    private Stage editStage;
    private Button editRoom;
    private Scene editScene;
    private GraphicsContext dungeonGraphics;
    private GridPane specialButtonPane;
    /**
     * The globally defined height of the program.
     */
    private static final int HEIGHT = 700;
    /**
     * The globally defined width of the program.
     */
    private static final int WIDTH = 700;
    private ListView<CustomChoiceCell> monsterView;
    private ListView<CustomChoiceCell> treasureView;
    private TextArea mainText;

    /**
     * Runs in a loop, generating for the user.
     *
     * @param dungeonStage Is the stage used to display everything
     */
    @Override
    public void start(Stage dungeonStage) {
        //Setting up buttons & text window
        stageController = new GuiController();
        primaryStage = dungeonStage;
        //The edit pane
        createEditPane();
        //The main pane
        AnchorPane rootPane = createRootPane();
        Scene chooseGeneration = new Scene(rootPane, WIDTH, HEIGHT); //just using a basic resolution for now
        dungeonStage.setTitle("Dungeon Generator");
        dungeonStage.setScene(chooseGeneration);
        dungeonStage.show();
    }

    /**
     * Builds the screen as default, using a passed in pane.
     *
     * @return Anchorpane is the initialized pane
     */
    private AnchorPane createRootPane() {
        AnchorPane contentPane = new AnchorPane();
        TextFlow textField = createTextBox();
        Canvas dungeonCanvas = createCanvas();
        dungeonGraphics = dungeonCanvas.getGraphicsContext2D();
        editRoom = new Button();
        specialButtonPane = createButtonPane();
        ListView<LevelComponent> generatedDoors = new ListView<>();
        ListView<LevelComponent> generatedSpaces = createSpaceView(generatedDoors);
        createDoorView(generatedDoors, generatedSpaces);
        editRoom = createEditButton(generatedSpaces);
        MenuBar fileLoadBar = createMenus(generatedSpaces);
        contentPane.getChildren().addAll(textField, editRoom, dungeonCanvas, specialButtonPane, generatedSpaces, generatedDoors, fileLoadBar);
        return contentPane;
    }

    private GridPane createButtonPane() {
        GridPane buttons = new GridPane();
        buttons.setPrefSize(WIDTH / 5.0, HEIGHT / 1.0);
        buttons.setTranslateX(WIDTH - WIDTH / 5.0 - WIDTH / 30.0);
        buttons.setTranslateY(HEIGHT / 1.8 + HEIGHT / 8.0);
        return buttons;
    }

    /**
     * Edits the currently selected space, changing monsters or treasure.
     */
    private void createEditPane() {
        AnchorPane tempEditPane = new AnchorPane();
        monsterView = createMonsterListView();
        treasureView = createTreasureListView();
        ChoiceBox<String> monsterList = createMonsterList();
        ChoiceBox<String> treasureList = createTreasureList();
        Button addTreasureButton = createAddTreasureButton(treasureList);
        Button addMonsterButton = createAddMonsterButton(monsterList);
        Label treasureLabel = createGenericLabel("Add Treasure", WIDTH / 6, HEIGHT - HEIGHT / 2);
        Label monsterLabel = createGenericLabel("Add Monster", WIDTH / 6, HEIGHT / 16);
        Label treasureRemove = createGenericLabel("Treasure List", WIDTH - WIDTH / 3, HEIGHT - HEIGHT / 2);
        Label monsterRemove = createGenericLabel("Monster List", WIDTH - WIDTH / 3, HEIGHT / 16);
        Button closePopUp = createCloseButton();
        tempEditPane.getChildren().addAll(monsterView, treasureView, monsterList, treasureList, addTreasureButton, addMonsterButton, closePopUp, treasureLabel, monsterLabel, treasureRemove, monsterRemove);
        editScene = new Scene(tempEditPane, WIDTH, HEIGHT);
    }

    private Canvas createCanvas() {
        Canvas tempCanvas = new Canvas(WIDTH / 2.0, HEIGHT / 3.6);
        GraphicsContext mapBg = tempCanvas.getGraphicsContext2D();
        mapBg.drawImage(new Image("file:res/mapbg.jpg"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        tempCanvas.setTranslateX(WIDTH / 2.0 - tempCanvas.getWidth() / 2);
        tempCanvas.setTranslateY(HEIGHT / 8.0 + mainText.getPrefHeight()); //Center the box
        return tempCanvas;
    }

    private ChoiceBox<String> createMonsterList() {
        ChoiceBox<String> monsterList = new ChoiceBox<>();
        monsterList.setPrefSize(WIDTH / 4.0, HEIGHT / 24.0);
        monsterList.setTranslateX(WIDTH / 6.0);
        monsterList.setTranslateY(HEIGHT / 10.0);
        HashSet<String> monsterStringList = new HashSet<>();
        for (int i = 1; i < 101; i++) {
            Monster newMonster = new Monster();
            newMonster.setType(i);
            if (!monsterStringList.contains(newMonster.getDescription())) {
                monsterList.getItems().add(newMonster.getDescription());
            }
            monsterStringList.add(newMonster.getDescription());
        }
        return monsterList;
    }

    private ChoiceBox<String> createTreasureList() {
        ChoiceBox<String> treasureList = new ChoiceBox<>();
        treasureList.setPrefSize(WIDTH / 4.0, HEIGHT / 24.0);
        treasureList.setTranslateX(WIDTH / 6.0);
        treasureList.setTranslateY(HEIGHT - HEIGHT / 2.2);
        HashSet<String> treasureStringList = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            Treasure newTreasure = new Treasure();
            newTreasure.chooseTreasure(i);
            if (!treasureStringList.contains(newTreasure.getDescription())) {
                treasureList.getItems().add(newTreasure.getDescription());
            }
            treasureStringList.add(newTreasure.getDescription());
        }
        return treasureList;
    }

    private Button createAddTreasureButton(ChoiceBox<String> treasureList) {
        Button createButton = createGenericButton("Add chosen treasure", WIDTH / 4.0, HEIGHT / 16.0);
        createButton.setTranslateX(WIDTH / 6.0);
        createButton.setTranslateY(HEIGHT - HEIGHT / 2.6);
        createButton.setOnAction(event -> addTreasure(treasureList));
        return createButton;
    }

    private Button createAddMonsterButton(ChoiceBox<String> monsterList) {
        Button createButton = createGenericButton("Add chosen monster", WIDTH / 4.0, HEIGHT / 16.0);
        createButton.setTranslateX(WIDTH / 6.0);
        createButton.setTranslateY(HEIGHT / 6.0);
        createButton.setOnAction(event -> addMonster(monsterList));
        return createButton;
    }

    private void addMonster(ChoiceBox<String> monsterList) {
        if (monsterList.getSelectionModel().getSelectedItem() != null) {
            stageController.addMonster(monsterList.getSelectionModel().getSelectedItem());
        }
        updateMonsterView();
    }

    private void addTreasure(ChoiceBox<String> treasureList) {
        if (treasureList.getSelectionModel().getSelectedItem() != null) {
            stageController.addTreasure(treasureList.getSelectionModel().getSelectedItem());
        }
        updateTreasureView();
    }

    private Label createGenericLabel(String setText, int xLocation, int yLocation) {
        Label genericLabel = new Label();
        genericLabel.setText(setText);
        genericLabel.setTranslateX(xLocation);
        genericLabel.setTranslateY(yLocation);
        return genericLabel;
    }

    private void setSceneEdit(Space givenSpace) {
        editStage = new Stage();
        editStage.setTitle("Edit current space");
        editStage.initModality(Modality.APPLICATION_MODAL);
        stageController.setSpace(givenSpace);
        updateMonsterView();
        updateTreasureView();
        editStage.initOwner(primaryStage);
        editStage.setScene(editScene);
        editStage.show();
    }

    private ListView<CustomChoiceCell> createMonsterListView() {
        monsterView = objectCreate();
        monsterView.setTranslateY(HEIGHT / 10.0);
        return monsterView;
    }

    private ListView<CustomChoiceCell> createTreasureListView() {
        treasureView = objectCreate();
        treasureView.setTranslateY(HEIGHT - HEIGHT / 2.2);
        return treasureView;
    }

    private ListView<CustomChoiceCell> objectCreate() {
        ListView<CustomChoiceCell> tempView = new ListView<>();
        ArrayList<CustomChoiceCell> allCells = new ArrayList<>();
        tempView.setItems(FXCollections.observableList(allCells));
        tempView.setPrefSize(WIDTH / 3.0, HEIGHT / 3.0);
        tempView.setTranslateX(WIDTH - WIDTH / 2.2);
        return tempView;
    }

    private void updateMonsterView() {
        ArrayList<CustomChoiceCell> allMonsterCells = new ArrayList<>();
        for (Monster m : stageController.getMonsters(stageController.getSpace())) {
            allMonsterCells.add(new CustomChoiceCell(m));
        }
        monsterView.setItems(FXCollections.observableList(allMonsterCells));
    }

    private void updateTreasureView() {
        ArrayList<CustomChoiceCell> allTreasureCells = new ArrayList<>();
        for (Treasure t : stageController.getTreasures(stageController.getSpace())) {
            allTreasureCells.add(new CustomChoiceCell(t));
        }
        treasureView.setItems(FXCollections.observableList(allTreasureCells));
    }

    private Button createCloseButton() {
        Button closePopUp = createGenericButton("Save and return", WIDTH / 3.0, HEIGHT / 16.0);
        closePopUp.setOnAction(event -> closeCurrentWindow());
        closePopUp.setTranslateX(WIDTH - closePopUp.getPrefWidth() - WIDTH / 36.0);
        closePopUp.setTranslateY(HEIGHT - closePopUp.getPrefHeight() - HEIGHT / 36.0);
        return closePopUp;
    }

    private void closeCurrentWindow() {
        editStage.close();
        mainText.setText(stageController.getSpace().getDescription());
        if (stageController.getSpace() instanceof Passage) {
            drawPassageShape(stageController.getSpace());
        } else {
            drawChamberShape(stageController.getSpace().getDescription());
        }
    }

    private TextFlow createTextBox() {
        mainText = new TextArea();
        mainText.setPrefSize(WIDTH / 2.0, HEIGHT / 3.6);
        mainText.setTranslateX(WIDTH / 2.0 - mainText.getPrefWidth() / 2);
        mainText.setTranslateY(HEIGHT / 8.0); //Center the box
        mainText.setWrapText(true);
        mainText.setText("Either load a file or generate a new one from the edit menu to begin.");
        TextFlow textField = new TextFlow();
        textField.getChildren().add(mainText);
        return textField;
    }

    private ListView<LevelComponent> createSpaceView(ListView<LevelComponent> generatedDoors) {
        ListView<LevelComponent> generatedSpaces = new ListView<>();
        generatedSpaces.setPrefSize(WIDTH / 5.0, HEIGHT / 1.8);
        generatedSpaces.setTranslateX(WIDTH / 30.0);
        generatedSpaces.setTranslateY(HEIGHT / 8.0);
        generatedSpaces.setOnMouseClicked(event -> listEvent(generatedSpaces, generatedDoors));
        return generatedSpaces;
    }

    private void createDoorView(ListView<LevelComponent> generatedDoors, ListView<LevelComponent> generatedSpaces) {
        generatedDoors.setPrefSize(WIDTH / 5.0, HEIGHT / 1.8);
        generatedDoors.setTranslateX(WIDTH - generatedDoors.getPrefWidth() - WIDTH / 30.0);
        generatedDoors.setTranslateY(HEIGHT / 8.0);
        generatedDoors.setOnMouseClicked(event -> listEvent(generatedDoors, generatedSpaces));
    }

    private void listEvent(ListView<LevelComponent> generatedSpaces, ListView<LevelComponent> generatedSecond) {
        specialButtonPane.getChildren().clear();
        if (generatedSpaces.getSelectionModel().getSelectedItem() != null) { //If user makes a valid selection
            mainText.setText(generatedSpaces.getSelectionModel().getSelectedItem().getDescription());
            if (generatedSpaces.getSelectionModel().getSelectedItem() instanceof Chamber) {
                stageController.setSpace((Space) generatedSpaces.getSelectionModel().getSelectedItem());
                drawChamberShape(generatedSpaces.getSelectionModel().getSelectedItem().getDescription());
            } else if (generatedSpaces.getSelectionModel().getSelectedItem() instanceof Passage) {
                stageController.setSpace((Space) generatedSpaces.getSelectionModel().getSelectedItem());
                drawPassageShape(generatedSpaces.getSelectionModel().getSelectedItem());
            }
            editRoom.setVisible(false);
            if (generatedSpaces.getSelectionModel().getSelectedItem() instanceof Space) {
                listFill(generatedSecond, generatedSpaces.getSelectionModel().getSelectedItem());
                editRoom.setVisible(true);
            } else {
                Door checkDoor = (Door) generatedSpaces.getSelectionModel().getSelectedItem();
                Button[] allButtons = new Button[2];
                for (int i = 0; i < checkDoor.getSpaces().size(); i++) {
                    if (checkDoor.getSpaces().get(i).getId() != null) {
                        allButtons[i] = createGenericButton(checkDoor.getSpaces().get(i).getId(), WIDTH / 5.0, HEIGHT / 32.0);
                        Space tempSpace = checkDoor.getSpaces().get(i);
                        allButtons[i].setOnAction(event -> returnButton(tempSpace, generatedSecond, generatedSpaces));
                        specialButtonPane.add(allButtons[i], 0, i * (i + (int) allButtons[i].getHeight()));
                    }
                }
            }
        }
    }

    private void returnButton(Space givenSpace, ListView<LevelComponent> generatedSpaces, ListView<LevelComponent> generatedSecond) {
        generatedSpaces.getSelectionModel().select(givenSpace);
        listEvent(generatedSpaces, generatedSecond);
        specialButtonPane.getChildren().clear();
    }

    private void drawChamberShape(String shapeDescription) {
        dungeonGraphics.clearRect(0, 0, WIDTH, HEIGHT); //clear the graphics for the new ones to be drawn
        dungeonGraphics.drawImage(new Image("file:res/mapbg.jpg"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        dungeonGraphics.setFill(Color.GRAY);
        if (shapeDescription.contains("Rectangle")) {
            dungeonGraphics.drawImage(new Image("file:res/rectangleShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Square")) {
            dungeonGraphics.drawImage(new Image("file:res/squareShape.png"), (WIDTH / 4.0 - WIDTH / 7.2), 0, WIDTH / 3.6, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Triangular")) {
            dungeonGraphics.drawImage(new Image("file:res/triangleShape.png"), (WIDTH / 4.0 - WIDTH / 7.2), 0, WIDTH / 3.6, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Circular")) {
            dungeonGraphics.drawImage(new Image("file:res/circleShape.png"), (WIDTH / 4.0 - WIDTH / 7.2), 0, WIDTH / 3.6, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Trapezoidal")) {
            dungeonGraphics.drawImage(new Image("file:res/trapezoidShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Custom Shape")) {
            dungeonGraphics.drawImage(new Image("file:res/customShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Oval")) {
            dungeonGraphics.drawImage(new Image("file:res/ovalShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Hexagonal")) {
            dungeonGraphics.drawImage(new Image("file:res/hexagonShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Octagonal")) {
            dungeonGraphics.drawImage(new Image("file:res/octagonShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        } else if (shapeDescription.contains("Cave")) {
            dungeonGraphics.drawImage(new Image("file:res/caveShape.png"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        }
        for (Monster ignored : stageController.getMonsters(stageController.getSpace())) {
            placeImage("file:res/monster.png");
        }
        for (Treasure ignored : stageController.getTreasures(stageController.getSpace())) {
            placeImage("file:res/treasure.png");
        }
        for (int i = 0; i < stageController.getSpace().getNumDoors(); i++) {
            double circleCoords = (Math.PI / 2) + (i * Math.PI / 2);
            double xPos = (WIDTH / 4.0 - WIDTH / 48.0) + (Math.cos(circleCoords) * WIDTH / 6.0);
            double yPos = (HEIGHT / 7.2 - HEIGHT / 24.0) + (Math.sin(circleCoords) * HEIGHT / 12.0);
            dungeonGraphics.drawImage(new Image("file:res/door.jpg"), xPos, yPos, WIDTH / 24.0, HEIGHT / 12.0);
        }
    }

    private void placeImage(String url) {
        Random randomPlace = new Random();
        double xPos = randomPlace.nextDouble() * (WIDTH / 2.0);
        if (xPos < WIDTH / 4.0) {
            xPos += WIDTH / 15.0;
        } else {
            xPos -= WIDTH / 15.0;
        }
        double yPos = randomPlace.nextDouble() * (HEIGHT / 3.6);
        if (yPos < HEIGHT / 7.2) {
            yPos += HEIGHT / 15.0;
        } else {
            yPos -= HEIGHT / 15.0;
        }
        dungeonGraphics.drawImage(new Image(url), xPos, yPos, WIDTH / 15.0, HEIGHT / 15.0);
    }

    private void drawPassageShape(LevelComponent mySpace) {
        Passage fullPassage = (Passage) mySpace;
        dungeonGraphics.clearRect(0, 0, WIDTH, HEIGHT); //clear the graphics for the new ones to be drawn
        dungeonGraphics.drawImage(new Image("file:res/mapbg.jpg"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
        dungeonGraphics.setFill(Color.GRAY);
        double xPos = WIDTH / 4.0 - WIDTH / 30.0;
        double yPos = HEIGHT / 3.6 - WIDTH / 15.0;
        double squareWidth = WIDTH / 15.0;
        int direction = -1; //x mod 4: 1 for east, 2 for south, 3 for west, 0 for north
        Image passageSquare = new Image("file:res/squareShape.png");
        for (PassageSection p : fullPassage.getPassageSections()) {
            drawFinalShape(xPos, yPos, squareWidth, squareWidth, passageSquare, 0);
            if (direction == -1) {
                drawFinalShape(xPos + WIDTH / 30.0 - squareWidth / 3.0, yPos + WIDTH / 15.0 - squareWidth / 10.0, squareWidth / 1.5, squareWidth / 5.0, new Image("file:res/door.jpg"), 90);
                direction = 0;
            }
            if (p.getMonsters().size() > 0) {
                dungeonGraphics.drawImage(new Image("file:res/monster.png"), xPos, yPos, squareWidth, squareWidth);
            }
            if (p.getTreasures().size() > 0) {
                dungeonGraphics.drawImage(new Image("file:res/treasure.png"), xPos, yPos, squareWidth, squareWidth);
            }
            if (p.getDescription().contains("archway") || p.getDescription().contains("door")) {
                Image passageDoor = new Image("file:res/door.jpg");
                if ((direction % 4 == 0 && p.getDescription().contains("archway to the left")) || (direction % 4 == 2 && p.getDescription().contains("archway to the right")) || (direction % 4 == 3) && p.getDescription().contains("to a chamber")) {
                    drawFinalShape(xPos - squareWidth / 10.0, yPos + WIDTH / 30.0 - squareWidth / 3.0, squareWidth / 5.0, squareWidth / 1.5, passageDoor, 0);
                } else if (direction % 4 == 1 && p.getDescription().contains("archway to the left") || (direction % 4 == 3 && p.getDescription().contains("archway to the right")) || (direction % 4 == 0) && p.getDescription().contains("to a chamber")) {
                    drawFinalShape(xPos + WIDTH / 30.0 - squareWidth / 3.0, yPos - squareWidth / 10.0, squareWidth / 1.5, squareWidth / 5.0, passageDoor, 90);
                } else if (direction % 4 == 2 && p.getDescription().contains("archway to the left") || (direction % 4 == 0 && p.getDescription().contains("archway to the right")) || (direction % 4 == 1) && p.getDescription().contains("to a chamber")) {
                    drawFinalShape(xPos + WIDTH / 15.0 - squareWidth / 10.0, yPos + WIDTH / 30.0 - squareWidth / 3.0, squareWidth / 5.0, squareWidth / 1.5, passageDoor, 0);
                } else if (direction % 4 == 3 && p.getDescription().contains("archway to the left") || (direction % 4 == 1 && p.getDescription().contains("archway to the right")) || (direction % 4 == 2) && p.getDescription().contains("to a chamber")) {
                    drawFinalShape(xPos + WIDTH / 30.0 - squareWidth / 3.0, yPos + WIDTH / 15.0 - squareWidth / 10.0, squareWidth / 1.5, squareWidth / 5.0, passageDoor, 90);
                }
            }
            if (p.getDescription().contains("veers to the right")) {
                direction++;
            } else if (p.getDescription().contains("veers to the left")) {
                direction--;
                if (direction == -1) {
                    direction = 3;
                }
            }
            if (direction % 4 == 0) {
                yPos -= squareWidth;
            } else if (direction % 4 == 1) {
                xPos += squareWidth;
            } else if (direction % 4 == 2) {
                yPos += squareWidth;
            } else if (direction % 4 == 3) {
                xPos -= squareWidth;
            }
        }
    }

    private void drawFinalShape(double xPos, double yPos, double width, double height, Image imToRotate, double rotation) {
        ImageView rotateDoor = new ImageView(imToRotate);
        rotateDoor.setRotate(rotation);
        SnapshotParameters backToImage = new SnapshotParameters();
        backToImage.setFill(Color.TRANSPARENT);
        imToRotate = rotateDoor.snapshot(backToImage, null);
        dungeonGraphics.drawImage(imToRotate, xPos, yPos, width, height);
    }

    private MenuBar createMenus(ListView<LevelComponent> spaceComponent) {
        MenuBar fileLoadBar = new MenuBar();
        fileLoadBar.setUseSystemMenuBar(true);
        Menu fileOption = createFileOption(spaceComponent);
        Menu editOption = createEditOption(spaceComponent);
        fileLoadBar.getMenus().addAll(fileOption, editOption);
        return fileLoadBar;
    }

    private Menu createFileOption(ListView<LevelComponent> spaceComponent) {
        Menu fileOption = new Menu();
        fileOption.setText("File");
        MenuItem fileLoad = new MenuItem();
        MenuItem fileSave = new MenuItem();
        fileLoad.setText("Load from file");
        fileSave.setText("Save to file");
        FileChooser.ExtensionFilter objectFilter = new FileChooser.ExtensionFilter("Level files (*.lev)", "*.lev");
        fileOption.getItems().addAll(fileLoad, fileSave);
        fileLoad.setOnAction((ActionEvent event) -> readCurrentFile(objectFilter, spaceComponent));
        fileSave.setOnAction((ActionEvent event) -> saveCurrentFile(objectFilter));
        return fileOption;
    }

    private void saveCurrentFile(FileChooser.ExtensionFilter objectFilter) {
        FileChooser saveFile = new FileChooser();
        saveFile.setSelectedExtensionFilter(objectFilter);
        saveFile.setTitle("Save object file");
        saveFile.setInitialFileName("Floor.lev");
        File floorObject = saveFile.showSaveDialog(new Stage());
        try {
            FileOutputStream givenFile = new FileOutputStream(floorObject);
            ObjectOutputStream obToFile = new ObjectOutputStream(givenFile);
            stageController.saveFloor(obToFile);
        } catch (IOException e) {
            System.out.println("Couldn't save the selected file");
        }
    }

    private void readCurrentFile(FileChooser.ExtensionFilter objectFilter, ListView<LevelComponent> spaceComponent) {
        FileChooser findFile = new FileChooser();
        findFile.setSelectedExtensionFilter(objectFilter);
        findFile.setTitle("Open object file");
        findFile.setInitialFileName("Floor.lev");
        File floorObject = findFile.showOpenDialog(new Stage());
        try {
            FileInputStream givenFile = new FileInputStream(floorObject);
            ObjectInputStream fileToOb = new ObjectInputStream(givenFile);
            stageController.setFloor((Floor) fileToOb.readObject());
            listFill(spaceComponent);
            clearScreen();
        } catch (IOException e) {
            System.out.println("Incorrect file.");
        } catch (ClassNotFoundException e) {
            System.out.println("Wrong class type");
        }
    }

    private Menu createEditOption(ListView<LevelComponent> spaceComponent) {
        Menu editOption = new Menu();
        editOption.setText("Edit");
        MenuItem generate = new MenuItem();
        generate.setText("Generate a new level");
        editOption.getItems().add(generate);
        generate.setOnAction((ActionEvent event) -> {
            stageController.regenerateFloor();
            listFill(spaceComponent);
            clearScreen();
        });
        return editOption;
    }

    private Button createEditButton(ListView<LevelComponent> generatedSpaces) {
        editRoom.setText("Edit");
        editRoom.setPrefSize(WIDTH / 5.0, HEIGHT / 10.0);
        editRoom.setTranslateX(generatedSpaces.getTranslateX());
        editRoom.setTranslateY(generatedSpaces.getTranslateY() + generatedSpaces.getPrefHeight() + HEIGHT / 20.0);
        editRoom.setVisible(false);
        editRoom.setOnAction(event -> setSceneEdit((Space) generatedSpaces.getSelectionModel().getSelectedItem()));
        return editRoom;
    }

    private void clearScreen() {
        mainText.setText("");
        dungeonGraphics.clearRect(0, 0, WIDTH, HEIGHT); //clear the graphics for the new ones to be drawn
        dungeonGraphics.drawImage(new Image("file:res/mapbg.jpg"), 0, 0, WIDTH / 2.0, HEIGHT / 3.6);
    }

    private Button createGenericButton(String label, double width, double length) {
        Button genButton = new Button();
        genButton.setText(label);
        genButton.setPrefSize(width, length);
        return genButton;
    }

    /**
     * Fills in the list view box.
     *
     * @param givenView is the list view passed in
     * @param doorSpace is an optional list of spaces.
     */
    private void listFill(ListView<LevelComponent> givenView, LevelComponent... doorSpace) {
        givenView.getItems().clear();
        if (doorSpace.length == 0) {
            givenView.getItems().addAll(stageController.getSpaces());
        } else {
            givenView.getItems().addAll(doorSpace[0].getDoors());
        }
        givenView.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(LevelComponent givenSpace, boolean empty) {
                super.updateItem(givenSpace, empty);
                if (empty || givenSpace == null || givenSpace.getId() == null) {
                    setText(null);
                } else {
                    setText(givenSpace.getId());
                }
            }
        });
    }

    private class CustomChoiceCell extends HBox {

        CustomChoiceCell(DnDElement givenElement) {
            super();
            Label modelDesc = new Label();
            Button remove = new Button();
            modelDesc.setText(givenElement.getDescription());
            modelDesc.setMaxWidth(Double.MAX_VALUE); //shift the button all the way over
            HBox.setHgrow(modelDesc, Priority.ALWAYS);
            remove.setText("X");
            remove.setOnAction(event -> {
                stageController.removeElement(givenElement);
                this.setVisible(false);
            });
            getChildren().addAll(modelDesc, remove);
        }
    }

    /**
     * Runs the program.
     * @param args is the arguments (unused).
     */
    public static void main(String[] args) {
        launch(args);
    }
}
