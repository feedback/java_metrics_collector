package s1;

import dnd.die.D20;
import dnd.die.Percentile;
import dnd.exceptions.UnusualShapeException;
import dnd.models.*;

import java.util.ArrayList;

/**
 * This file contains the constructor for the
 * Chamber class, which contains all of the
 * room elements as defined in other sections of
 * the dnd.models package.
 *
 * @author s1
 * @version 1.2
 * @since 2019-10-05
 */
public class Chamber extends Space {
    /**
     * A stored object containing a string representation of room contents.
     */
    private ChamberContents chamberContents;
    /**
     * A stored object containing the shape details.
     */
    private ChamberShape chamberShape;
    /**
     * A stored object containing a list of monsters (should only have 1).
     */
    private ArrayList<Monster> chamberMonsters;
    /**
     * A stored object containing a list of treasures (should only have 1).
     */
    private ArrayList<Treasure> chamberTreasures;
    /**
     * A stored object representing the possible trap.
     */
    private Trap chamberTrap;
    /**
     * A stored object representing the possible stairs.
     */
    private Stairs chamberStairs;
    /**
     * A variable that stores the collection of doors.
     */
    private ArrayList<Door> chamberDoors;
    /**
     * A variable containing the only door for the passage.
     */
    private Door chamberDoor;

    /**
     * This is the basic Chamber constructor, which simply creates space for the
     * Door, ChamberShape, ChamberContents, and HashMap (all of which are mandatory elements).
     */
    Chamber() {
        addContents(new ChamberContents());
        setId("Chamber");
        D20 d20Roller = new D20();
        chamberShape = ChamberShape.selectChamberShape(d20Roller.roll());
        chamberShape.setNumExits(d20Roller.roll());
        buildChamber();
        setShape(chamberShape);
    }

    /**
     * This function builds the chamber according to table rolls.
     * Also fills in a table with door values.
     */
    private void buildChamber() {
        chamberDoors = new ArrayList<>();
        chamberMonsters = new ArrayList<>();
        chamberTreasures = new ArrayList<>();
        if (chamberContents.getDescription().compareTo("monster only") == 0) {
            addMonster(new Monster());
        } else if (chamberContents.getDescription().compareTo("monster and treasure") == 0) {
            addTreasure(new Treasure());
            addMonster(new Monster());
        } else if (chamberContents.getDescription().compareTo("stairs") == 0) {
            setStairs(new Stairs());
        } else if (chamberContents.getDescription().compareTo("trap") == 0) {
            setTrap(new Trap());
        } else if (chamberContents.getDescription().compareTo("treasure") == 0) {
            addTreasure(new Treasure());
        }
    }

    /**
     * Returns every door associated with the room.
     *
     * @return An arraylist of doors.
     */
    @Override
    public ArrayList<LevelComponent> getDoors() {
        ArrayList<LevelComponent> doorList = new ArrayList<>();
        doorList.add(chamberDoor);
        for (Door d : chamberDoors) {
            if (d.getSpaces().size() > 0) {
                doorList.add(d);
            }
        }
        return doorList;
    }

    /**
     * setShape sets the shape of the chamber,
     * and in doing so also creates a new default door for each generated exit.
     *
     * @param theShape Takes in a previously-generated chambershape and sets exits accordingly.
     */
    private void setShape(ChamberShape theShape) {
        chamberShape = theShape;
        for (int i = 0; i < chamberShape.getNumExits(); i++) {
            chamberDoors.add(new Door());
            chamberDoors.get(chamberDoors.size() - 1).setPassage(i + 1);
        }
    }

    /**
     * addContents rolls for the content.
     *
     * @param theChamber is the chamber to be made.
     */
    private void addContents(ChamberContents theChamber) {
        D20 contentRoller = new D20();
        theChamber.chooseContents(contentRoller.roll());
        chamberContents = theChamber;
    }

    /**
     * String getContents returns a string.
     * @return a string containing the contents description.
     */
    String getContents() {
        return chamberContents.getDescription();
    }

    /**
     * setContents sets contents based on a provided roll.
     * @param roll is the roll to use.
     */
    void setContents(int roll) {
        chamberContents.chooseContents(roll);
    }

    /**
     * addMonster adds a monster to the monster array.
     *
     * @param theMonster passes the monster to be added.
     */
    @Override
    public void addMonster(Monster theMonster, int... i) {
        if (i.length == 0) {
            Percentile monsterRoller = new Percentile();
            theMonster.setType(monsterRoller.roll());
        }
        chamberMonsters.add(theMonster);
    }

    /**
     * addTreasure adds a treasure to the treasure array.
     *
     * @param theTreasure passes the treasure to be added.
     */
    @Override
    public void addTreasure(Treasure theTreasure, int... i) {
        D20 protectionRoller = new D20();
        theTreasure.setContainer(protectionRoller.roll());
        if (i.length == 0) {
            Percentile treasureRoller = new Percentile();
            theTreasure.chooseTreasure(treasureRoller.roll());
        }
        chamberTreasures.add(theTreasure);
    }

    /**
     * setStairs sets the chamber's stairs.
     *
     * @param theStairs passes the stairs to be set.
     */
    private void setStairs(Stairs theStairs) {
        D20 d20 = new D20();
        theStairs.setType(d20.roll());
        chamberStairs = theStairs;
    }

    /**
     * setTrap sets the chamber's trap.
     *
     * @param theTrap passes the trap to be set.
     */
    private void setTrap(Trap theTrap) {
        D20 d20 = new D20();
        theTrap.chooseTrap(d20.roll());
        chamberTrap = theTrap;
    }

    /**
     * Returns the monsters.
     *
     * @return Monsters.
     */
    @Override
    public ArrayList<Monster> getMonsters() {
        return chamberMonsters;
    }

    /**
     * Removes the monsters.
     * @param m is the monster to remove.
     */
    @Override
    public void removeMonster(Monster m) {
        getMonsters().remove(m);
    }

    /**
     * Removes the treasures.
     * @param t is the treasure to remove.
     */
    @Override
    public void removeTreasure(Treasure t) {
        getTreasures().remove(t);
    }

    /**
     * Returns the treasures.
     *
     * @return Treasures.
     */
    @Override
    public ArrayList<Treasure> getTreasures() {
        return chamberTreasures;
    }

    /**
     * setDoor sets the entrance door of the chamber.
     *
     * @param newDoor the value of the door to set to entranceDoor.
     */
    @Override
    public void setDoor(Door newDoor) {
        if (chamberDoor == null) {
            chamberDoor = newDoor;
        }
    }

    /**
     * Returns a single door corresponding to a particular exit.
     *
     * @param exitIndex exitIndex is the corresponding section where the door is located.
     * @return Door at given section.
     */
    @Override
    public Door getDoor(int exitIndex) {
        if (exitIndex == -1) {
            return chamberDoor;
        }
        if (exitIndex < chamberDoors.size()) {
            return chamberDoors.get(exitIndex);
        }
        return null;
    }

    /**
     * This method returns the number of doors (excluding entrance door).
     *
     * @return An int representation of the number of doors.
     */
    @Override
    public int getNumDoors() {
        return chamberDoors.size();
    }

    /**
     * setExitDoor looks for an unused door in the chamber.
     * If it finds one, it activates it and attaches it to an exit.
     *
     * @return The int value the index of the exit, or -1 if no door was found.
     */
    @Override
    public int setExitDoor() { //looks for an exit without a set door - returns the equivilant int in the array
        int i;
        for (i = 0; i < chamberDoors.size(); i++) {
            if (!chamberDoors.get(i).getActive()) {
                Passage nextPassage = new Passage();
                nextPassage.buildPassage();
                chamberDoors.get(i).setSpaces(this, nextPassage);
                return i;
            }
        }
        return -1;
    }

    /**
     * getShapeDescription is a string creation helper.
     * It uses the values in the chamberShape to return a description.
     *
     * @return a string representation of the shape.
     */
    private String getShapeDescription() {
        String returnString = "";
        returnString = returnString.concat("The connected chamber is " + chamberShape.getShape() + "-shaped.\n");
        try {
            returnString = returnString.concat("It is " + chamberShape.getWidth() + "' by " + chamberShape.getLength() + "'.\n");
        } catch (UnusualShapeException e) {
            returnString = returnString.concat(e.getMessage() + ".\n");
        }
        returnString = returnString.concat("It has an area of " + chamberShape.getArea() + " square feet.\n");
        returnString = returnString.concat("There are " + chamberShape.getNumExits() + " exits.\n");
        return returnString;
    }

    /**
     * getContentsDescription is a string creation helper.
     * It uses the value in chambercontents to return a description.
     *
     * @return a string representation of the room.
     */
    private String getContentsDescription() {
        String returnString = "";
        if (chamberContents.getDescription().contains("empty")) {
            returnString = returnString.concat("The room is empty.\n");
        } else {
            returnString = returnString.concat("The room contains a " + chamberContents.getDescription() + ".\n");
        }
        return returnString;
    }

    /**
     * getTreasureDescription is a string creation helper.
     * It uses the values in the chamberTreasure to return a description.
     *
     * @return a string representation of the treasure.
     */
    private String getTreasureDescription() {
        String returnString = "";
        for (Treasure chamberTreasure : chamberTreasures) {
            returnString = returnString.concat("The treasure is: " + chamberTreasure.getWholeDescription() + ".\n");
        }
        return returnString;
    }

    /**
     * getMonsterDescription is a string creation helper.
     * It uses the values in the chamberMonster to return a description.
     *
     * @return a string representation of the monster.
     */
    private String getMonsterDescription() {
        String returnString = ""; //start it empty
        for (Monster m : chamberMonsters) {
            returnString = returnString.concat("The monster is a(n) " + m.getDescription() + ".\n");
        }
        return returnString;
    }

    /**
     * getTrapDescription is a string creation helper.
     * It uses the values in the chamberTrap to return a description.
     *
     * @return a string representation of the trap.
     */
    private String getTrapDescription() {
        String returnString = "";
        if (chamberTrap != null) {
            returnString = "The trap is a " + chamberTrap.getDescription() + ".\n";
        }
        return returnString;
    }

    /**
     * getStairsDescription is a string creation helper.
     * It uses the values in the chamberStairs to return a description.
     *
     * @return a string representation of the stairs.
     */
    private String getStairsDescription() {
        String returnString = "";
        if (chamberStairs != null) {
            returnString = "The stairs go " + chamberStairs.getDescription() + ".\n";
        }
        return returnString;
    }

    /**
     * getDescription compiles a string description of the room.
     *
     * @return A string representation of the room.
     */
    @Override
    public String getDescription() {
        String chamberDescription = "";
        chamberDescription = chamberDescription.concat(getShapeDescription());
        chamberDescription = chamberDescription.concat(getContentsDescription());
        chamberDescription = chamberDescription.concat(getTreasureDescription());
        chamberDescription = chamberDescription.concat(getMonsterDescription());
        chamberDescription = chamberDescription.concat(getTrapDescription());
        chamberDescription = chamberDescription.concat(getStairsDescription());
        return chamberDescription;
    }
}
