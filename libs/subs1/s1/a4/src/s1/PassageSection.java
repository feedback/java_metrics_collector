package s1;

import dnd.die.D20;
import dnd.die.Percentile;
import dnd.models.Monster;
import dnd.models.Stairs;
import dnd.models.Treasure;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This is the passagesection class of the ajetleb package. Utility is to set any individual passagesection for the whole passage.
 * @author s1
 * @version 1.2
 * @since 2019-09-30
 */
class PassageSection implements Serializable {
    /**
     *  Stores a single monster possibly located in the section.
     */
    private ArrayList<Monster> passageMonsters;
    private ArrayList<Treasure> passageTreasures;
    /**
     *  Stores a single door possibly located in the section.
     */
    private Door passageDoor;
    /**
     *  Stores a single stairs possible located in the section.
     */
    private Stairs passageStairs;
    /**
     *  Stores the passage # of the current section.
     */
    private int sectionNumber;
    /**
     *  Stores a string for the contents of the section.
     */
    private String setDescription;
    /**
     *  A default constructor that makes the most basic type of passage.
     */
    private PassageSection() {
        passageMonsters = new ArrayList<>();
        passageTreasures = new ArrayList<>();
        passageDoor = null;
        passageStairs = null;
        sectionNumber = -1;
        setDescription = "The passage continues straight for 10 feet.";
    }
    /**
     *  A constructor that builds the chamber based on the string from the table given.
     *  @param description description is the table string value.
     *  @param secNum is the section number of the passagesection
     */
    PassageSection(String description, int secNum) {
        this(); //Call default constructor first, then change
        setDescription = description;
        setSection(secNum);
        if (setDescription.contains("door") || setDescription.contains("Door")) {
            setDoor(new Door(), false);
        }
        if (setDescription.contains("archway") || setDescription.contains("Archway")) {
            setDoor(new Door(), true);
        }
        if (setDescription.contains("treasure") || setDescription.contains("Treasure")) {
            addTreasure(new Treasure());
        }
        if (setDescription.contains("monster") || setDescription.contains("Monster")) {
            addMonster(new Monster());
        }
    }
    /**
     *  Sets the door.
     *  @param setDoor is the door to set.
     *  @param isArchway is the boolean representation of the archway state.
     */
    private void setDoor(Door setDoor, boolean isArchway) {
        passageDoor = setDoor;
        if (isArchway) {
            passageDoor.setArchway(0);
        } else {
            passageDoor.setArchway(5);
        }
        passageDoor.setPassage(getSection());
    }
    /**
     *  Returns a door stored (can be null).
     *  @return A door.
     */
    Door getDoor() {
        return passageDoor;
    }
    /**
     *  Sets the monster.
     *  @param setMonster is the door to set.
     *  @param roll is a variable length list of rolls, 1 or 0
     */
    void addMonster(Monster setMonster, int... roll) {
        if (roll.length == 0) {
            Percentile d100 = new Percentile();
            setMonster.setType(d100.roll());
        }
        passageMonsters.add(setMonster);
    }
    /**
     *  Returns a treasures stored (can be null).
     *  @return A treasures.
     */
    ArrayList<Treasure> getTreasures() {
        return passageTreasures;
    }
    void addTreasure(Treasure setTreasure, int... roll) {
        if (roll.length == 0) {
            D20 d20 = new D20();
            setTreasure.chooseTreasure(d20.roll());
        }
        passageTreasures.add(setTreasure);
    }
    /**
     *  Returns a Monster stored (can be null).
     *  @return A Monster.
     */
    ArrayList<Monster> getMonsters() {
        return passageMonsters;
    }
    /**
     *  Sets the Stairs.
     *  @param setStairs is the door to set.
     */
    private void setStairs(Stairs setStairs) {
        D20 d20 = new D20();
        setStairs.setType(d20.roll());
        passageStairs = setStairs;
    }
    /**
     *  Returns a Stairs stored (can be null).
     *  @return A Stairs.
     */
    Stairs getStairs() {
        return passageStairs;
    }
    /**
     *  Sets the section number.
     *  @param setNum is the number to set.
     */
    private void setSection(int setNum) {
        sectionNumber = setNum;
    }
    /**
     *  Returns the section number.
     *  @return A section number.
     */
    private int getSection() {
        return sectionNumber;
    }
    /**
     *  getDescription compiles a string description of the singular section.
     *  @return A string representation of the room.
     */
    String getDescription() {
        return setDescription;
    }
}
