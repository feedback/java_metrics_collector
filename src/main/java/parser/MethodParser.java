package parser;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import metrics.MethodMetrics;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parses the csv file containing method data, then returns an ArrayList of {@link MethodMetrics MethodMetrics}.
 * The expected file is part of the output from ck and is called method.csv.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class MethodParser implements Parser<MethodMetrics> {

    @Override
    public ArrayList<MethodMetrics> parse(String filepath) throws Exception {
        ArrayList<MethodMetrics> list = new ArrayList<>();
        CSVReader reader = new CSVReaderBuilder(new FileReader(filepath)).withSkipLines(1).build();
        List<String[]> rows = reader.readAll();
        reader.close();

        for (String[] row : rows) {
            int[] data = Arrays.stream(Arrays.stream(row, 4, row.length).toArray(String[]::new))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            list.add(new MethodMetrics(row[0], row[1], row[2], Boolean.parseBoolean(row[3]),
                    data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9],
                    data[10], data[11], data[12], data[13], data[14], data[15], data[16], data[17], data[18],
                    data[19], data[20], data[21], data[22]));
        }
        return list;
    }
}
