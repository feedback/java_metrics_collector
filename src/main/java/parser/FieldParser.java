package parser;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import metrics.FieldMetrics;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses the csv file containing field data, then returns an ArrayList of {@link FieldMetrics FieldMetrics}.
 * The expected file is part of the output from ck and is called field.csv.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class FieldParser implements Parser<FieldMetrics> {

    @Override
    public ArrayList<FieldMetrics> parse(String filepath) throws Exception {
        ArrayList<FieldMetrics> list = new ArrayList<>();
        CSVReader reader = new CSVReaderBuilder(new FileReader(filepath)).withSkipLines(1).build();
        List<String[]> rows = reader.readAll();
        reader.close();

        for (String[] row : rows) {
            list.add(new FieldMetrics(row[0], row[1], row[2], row[3], Integer.parseInt(row[4])));
        }
        return list;
    }
}
