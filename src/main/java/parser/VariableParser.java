package parser;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import metrics.VariableMetrics;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses the csv file containing variable data, then returns an ArrayList of {@link VariableMetrics VariableMetrics}.
 * The expected file is part of the output from ck and is called variable.csv.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class VariableParser implements Parser<VariableMetrics> {

    @Override
    public ArrayList<VariableMetrics> parse(String filepath) throws Exception {
        ArrayList<VariableMetrics> list = new ArrayList<>();
        CSVReader reader = new CSVReaderBuilder(new FileReader(filepath)).withSkipLines(1).build();
        List<String[]> rows = reader.readAll();
        reader.close();

        for (String[] row : rows) {
            list.add(new VariableMetrics(row[0], row[1], row[2], row[3], Integer.parseInt(row[4])));
        }
        return list;
    }
}
