package parser;

import java.util.ArrayList;

/**
 * Defines a parser that takes the path to a file and returns
 * an ArrayList of data objects acquired from the file.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@FunctionalInterface
public interface Parser<T> {

    ArrayList<T> parse(String filepath) throws Exception;
}
