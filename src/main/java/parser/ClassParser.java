package parser;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import metrics.ClassMetrics;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parses the csv file containing class data, then returns an ArrayList of {@link ClassMetrics ClassMetrics}.
 * The expected file is part of the output from ck and is called class.csv.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class ClassParser implements Parser<ClassMetrics> {

    @Override
    public ArrayList<ClassMetrics> parse(String filepath) throws Exception {
        ArrayList<ClassMetrics> list = new ArrayList<>();
        CSVReader reader = new CSVReaderBuilder(new FileReader(filepath)).withSkipLines(1).build();
        List<String[]> rows = reader.readAll();
        reader.close();

        for (String[] row : rows) {
            int[] data = Arrays.stream(Arrays.stream(row, 3, row.length).toArray(String[]::new))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            list.add(new ClassMetrics(row[0], row[1], data[0], data[1], data[2], data[3], data[4],
                    data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14],
                    data[15], data[16], data[17], data[18], data[19], data[20], data[21], data[22], data[23],
                    data[24], data[25], data[26], data[27], data[28], data[29], data[30], data[31], data[32],
                    data[33], data[34], data[35], data[36], data[37], data[38]));
        }
        return list;
    }
}
