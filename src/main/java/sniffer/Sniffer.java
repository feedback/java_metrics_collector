package sniffer;

import metrics.TotalMetrics;
import smell.Smell;

/**
 * Interface for all {@link sniffer.Sniffer Sniffer} objects to be used for parsing {@link TotalMetrics TotalMetrics}
 * objects. Sniffers are used to search for code smells using data about a set of code. All Sniffers must implement a
 * sniff method which returns a {@link Smell Smell}. These must contain data on the smell name,
 * why it should be fixed, how to fix it, and the location of each smell, organized by {@link smell.Severity Severity}.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@FunctionalInterface
public interface Sniffer {

    Smell sniff(TotalMetrics totalMetrics);
}
