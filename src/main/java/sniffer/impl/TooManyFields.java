package sniffer.impl;

import metrics.ClassMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

/**
 * Searches for classes with too many fields.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class TooManyFields extends SettableSniffer {

    public TooManyFields(String className, String recommendation, String reasoning,
                            int minorThreshold, int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Too Many Fields", getRecommendation(), getReasoning());

        for (ClassMetrics classMetrics : totalMetrics.getClassMetrics()) {
            String location = classMetrics.getClassName();
            Severity severity = calculateSeverityAscending(classMetrics.getTotalFields());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
