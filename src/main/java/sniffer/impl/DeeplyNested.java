package sniffer.impl;

import metrics.MethodMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

/**
 * Searches for methods containing blocks which are nested too deeply.
 *
 * @author Shawn Kaplan
 * @since 2020-07-23
 */
public class DeeplyNested extends SettableSniffer {

    public DeeplyNested(String className, String recommendation, String reasoning,
                           int minorThreshold, int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Deeply Nested", getRecommendation(), getReasoning());

        for (MethodMetrics methodMetrics : totalMetrics.getMethodMetrics()) {
            String location = methodMetrics.getLocation();
            Severity severity = calculateSeverityAscending(methodMetrics.getMaxNestedBlocks());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
