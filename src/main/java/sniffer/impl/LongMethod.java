package sniffer.impl;

import metrics.MethodMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

/**
 * Searches for methods with too many lines of code.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public class LongMethod extends SettableSniffer {

    public LongMethod(String className,String recommendation, String reasoning,
                      int minorThreshold, int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Long Method", getRecommendation(), getReasoning());

        for (MethodMetrics methodMetrics : totalMetrics.getMethodMetrics()) {
            String location = methodMetrics.getLocation();
            Severity severity = calculateSeverityAscending(methodMetrics.getLinesOfCode());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
