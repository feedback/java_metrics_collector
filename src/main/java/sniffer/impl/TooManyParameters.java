package sniffer.impl;

import metrics.MethodMetrics;
import metrics.TotalMetrics;
import smell.Severity;
import smell.Smell;
import sniffer.SettableSniffer;

/**
 * Searches for methods with too many parameters.
 *
 * @author Shawn Kaplan
 * @since 2020-07-23
 */
public class TooManyParameters extends SettableSniffer {

    public TooManyParameters(String className, String recommendation, String reasoning,
                                int minorThreshold, int moderateThreshold, int majorThreshold) {

        super(className, recommendation, reasoning, minorThreshold, moderateThreshold, majorThreshold);
    }

    @Override
    public Smell sniff(TotalMetrics totalMetrics) {
        Smell smell = new Smell("Too Many Parameters", getRecommendation(), getReasoning());

        for (MethodMetrics methodMetrics : totalMetrics.getMethodMetrics()) {
            String location = methodMetrics.getLocation();
            Severity severity = calculateSeverityAscending(methodMetrics.getParameters());
            smell.addLocation(location, severity);
        }

        return smell;
    }
}
