package analyzer;

import com.github.mauricioaniche.ck.Runner;
import com.google.gson.GsonBuilder;
import metrics.TotalMetrics;
import parser.ClassParser;
import parser.FieldParser;
import parser.MethodParser;
import parser.VariableParser;
import smell.Smell;
import sniffer.SettableSnifferFactory;
import sniffer.Sniffer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Main class for the Java Metrics Analyzer program.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
public final class MetricsAnalyzer implements Analyzer {

    private ArrayList<Sniffer> sniffers = new ArrayList<>();
    private TotalMetrics totalMetrics;

    public MetricsAnalyzer(TotalMetrics totalMetrics) {
        this.totalMetrics = totalMetrics;
    }

    @Override
    public ArrayList<Smell> analyze() {
        ArrayList<Smell> smells = new ArrayList<>();
        for (Sniffer sniffer : sniffers) {
            Smell smell = sniffer.sniff(totalMetrics);
            if (smell.isFound()) {
                smells.add(smell);
            }
        }
        return smells;
    }

    public void addSniffer(Sniffer sniffer) {
        sniffers.add(sniffer);
    }

    public void addSniffers(Collection<Sniffer> sniffers) {
        this.sniffers.addAll(sniffers);
    }

    public ArrayList<Sniffer> getSniffers() {
        return sniffers;
    }

    public TotalMetrics getTotalMetrics() {
        return totalMetrics;
    }


    /**
     * Takes at least one argument from command line, the path to the directory containing code to analyze. The second
     * argument is the optional path to a custom settings.yaml file. If this flag is not given, a default is used.
     * 4 CSV files are created containing all of the metrics on the found code, which are then parsed for code smells.
     * <p>
     * Prints the name, solution, and reasoning behind each code smell found in the the given code, as well as
     * the location of each minor, moderate, and severe instance of that smell in a JSON file called JavaCodeSmells.json.
     *
     * @param args the path to the directory containing the code to be analyzed and the path to the settings yaml file
     * @throws Exception may be caused by failure to produce {@link TotalMetrics TotalMetrics} object
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Arguments: <path/to/code/> [path/to/settings]");
            System.exit(1);
        }

        String[] ckArgs = { args[0] };
        Runner.main(ckArgs);

        TotalMetrics totalMetrics = new TotalMetrics(
                new ClassParser().parse("class.csv"),
                new MethodParser().parse("method.csv"),
                new FieldParser().parse("field.csv"),
                new VariableParser().parse("variable.csv"));

        if (totalMetrics.isEmpty()) {
            System.err.println("FATAL: No Java code was found");
            System.exit(2);
        }

        MetricsAnalyzer analyzer = new MetricsAnalyzer(totalMetrics);
        InputStream inputStream;
        if (args.length < 2) {
            inputStream = MetricsAnalyzer.class.getClassLoader().getResourceAsStream("settings.yaml");
        } else {
            inputStream = new FileInputStream(args[1]);
        }
        analyzer.addSniffers(SettableSnifferFactory.createSniffers(inputStream));

        Analysis analysis = new Analysis(analyzer.analyze(), "Java Code Smells");
        saveSmells(new GsonBuilder().setPrettyPrinting().create().toJson(analysis));
        deleteMetrics();
    }

    private static void saveSmells(String smells) {
        final String FILENAME = "JavaCodeSmells.json";
        File file = new File(FILENAME);
        try {
            if (!file.createNewFile()) {
                System.out.println("Overwriting " + FILENAME);
            }
            FileWriter writer = new FileWriter(file);
            writer.write(smells);
            writer.close();
        } catch (Exception e) {
            System.out.println("FATAL: Failed to write to file");
            System.exit(3);
        }
    }

    private static void deleteMetrics() {
        try {
            Files.delete(new File("class.csv").toPath());
            Files.delete(new File("method.csv").toPath());
            Files.delete(new File("field.csv").toPath());
            Files.delete(new File("variable.csv").toPath());
        } catch (IOException e) {
            System.err.println("Failed to delete metrics files");
            e.printStackTrace();
        }
    }
}
