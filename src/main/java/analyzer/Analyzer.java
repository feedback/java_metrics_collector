package analyzer;

import smell.Smell;

import java.util.ArrayList;

/**
 * Defines an object which performs analysis to find code smells.
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@FunctionalInterface
public interface Analyzer {

    ArrayList<Smell> analyze();
}
