package metrics;

import lombok.Data;

/**
 * Contains class-level data acquired from class.csv.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Data
public class ClassMetrics implements Locatable {

    private final String fileName;
    private final String className;
    private final int couplingBetweenObjects;
    private final int complexity;
    private final int depthInheritanceTree;
    private final int responseForClass;
    private final int lackOfMethodCohesion;
    private final int totalMethods;
    private final int staticMethods;
    private final int publicMethods;
    private final int privateMethods;
    private final int protectedMethods;
    private final int defaultMethods;
    private final int abstractMethods;
    private final int finalMethods;
    private final int synchronizedMethods;
    private final int totalFields;
    private final int staticFields;
    private final int publicFields;
    private final int privateFields;
    private final int protectedFields;
    private final int defaultFields;
    private final int finalFields;
    private final int synchronizedFields;
    private final int staticInvocations;
    private final int linesOfCode;
    private final int returns;
    private final int loops;
    private final int comparisons;
    private final int tryCatches;
    private final int parenthesizedExpressions;
    private final int stringLiterals;
    private final int numbers;
    private final int assignments;
    private final int mathOps;
    private final int variables;
    private final int maxNestedBlocks;
    private final int anonymousClasses;
    private final int subclasses;
    private final int lambdas;
    private final int modifiers;

    @Override
    public String getLocation() {
        return getClassName();
    }
}
