package metrics;

/**
 * Interface for an object which has a location
 * (eg. a {@link ClassMetrics ClassMetrics's} location is the class's fully qualified name).
 */
public interface Locatable {

    String getLocation();
}
