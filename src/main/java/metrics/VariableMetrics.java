package metrics;

import lombok.Data;
import util.MethodNamePrettifier;

/**
 * Contains variable-level data acquired from variable.csv.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Data
public class VariableMetrics implements Locatable {

    private final String fileName;
    private final String className;
    private final String methodName;
    private final String variableName;
    private final int usage;

    @Override
    public String getLocation() {
        return getClassName() + "." + MethodNamePrettifier.prettify(getMethodName()) + " - " + getVariableName();
    }
}
