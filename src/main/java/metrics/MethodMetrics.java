package metrics;

import lombok.Data;
import util.MethodNamePrettifier;

/**
 * Contains method-level data acquired from method.csv.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/Data
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Data
public class MethodMetrics implements Locatable {

    private final String fileName;
    private final String className;
    private final String methodName;
    private final boolean constructor;
    private final int line;
    private final int couplingBetweenObjects;
    private final int complexity;
    private final int responseForClass;
    private final int linesOfCode;
    private final int returns;
    private final int variables;
    private final int parameters;
    private final int startLine;
    private final int loops;
    private final int comparisons;
    private final int tryCatches;
    private final int parentheses;
    private final int stringLiterals;
    private final int numbers;
    private final int assignments;
    private final int mathOps;
    private final int maxNestedBlocks;
    private final int anonymousClasses;
    private final int subClasses;
    private final int lambdas;
    private final int uniqueWords;
    private final int modifiers;

    @Override
    public String getLocation() {
        return getClassName() + "." + MethodNamePrettifier.prettify(getMethodName());
    }
}
