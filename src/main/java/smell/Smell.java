package smell;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * The name, solution, and reasoning for a type of code smell, as well as all locations where the smell was found,
 * organized by {@link smell.Severity Severity}.
 *
 * Lombok annotations used:
 * https://projectlombok.org/features/GetterSetter
 *
 * @author Shawn Kaplan
 * @since 2020-05-15
 */
@Getter
public class Smell {

    private String name;
    private String recommendation;
    private String reasoning;
    private List<String> majorLocations;
    private List<String> moderateLocations;
    private List<String> minorLocations;

    public Smell(String name, String recommendation, String reasoning) {
        majorLocations = new ArrayList<>();
        moderateLocations = new ArrayList<>();
        minorLocations = new ArrayList<>();

        this.name = name;
        this.recommendation = recommendation;
        this.reasoning = reasoning;
    }

    public void addLocation(String location, Severity severity) {
        switch(severity) {
            case MINOR:
                addMinorLocation(location);
                break;
            case MODERATE:
                addModerateLocation(location);
                break;
            case MAJOR:
                addMajorLocation(location);
                break;
        }
    }

    public void addMinorLocation(String location) {
        minorLocations.add(location);
    }

    public void addModerateLocation(String location) {
        moderateLocations.add(location);
    }

    public void addMajorLocation(String location) {
        majorLocations.add(location);
    }

    public int getCount() {
        return minorLocations.size() + moderateLocations.size() + majorLocations.size();
    }

    public boolean isFound() {
        return getCount() > 0;
    }
}
