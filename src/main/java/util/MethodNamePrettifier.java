package util;

/**
 * Converts the name of a method from "<methodName>/<numParams>[<param types>]" to "<methodName>(<numParams> params)"
 *
 * @author Shawn Kaplan
 * @since 2020-07-28
 */
public class MethodNamePrettifier {

    public static String prettify(String methodName) {
        int indexOfNumParams = methodName.indexOf("/") + 1;
        if (indexOfNumParams == 0) {
            return methodName;
        }

        int indexOfParams = methodName.indexOf("[");
        int numParams;
        try {
            numParams = Integer.parseInt(methodName.substring(indexOfNumParams, indexOfParams));
        } catch (Exception e) {
            numParams = 0;
        }

        return String.format("%s(%s)",
                methodName.substring(0, indexOfNumParams - 1),
                numParams + (numParams == 1 ? " param" : " params"));
    }
}
